"""
https://www.sharcnet.ca/Software/Abaqus610/Documentation/docs
/v6.10/books/cmd/default.htm?startat=pt05ch09s10.htmlodbMaxMises.py
Usage: abaqus python results_output.py CrossPly_S_quarter_5E-1
"""

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from odbAccess import *
from sys import argv,exit
import sys, os, re
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#==================================================================
# S T A R T
#

datapath = os.getcwd() + '/'
elsetName1 = 'P0-1'
elsetName2 = 'P90-1'
var = 'SDV11'
elem = 1

argList = argv
argc = len(argList)
i=0

fvar = sys.argv[1] + 'var.dat'
if os.path.exists(fvar):
    fio = open(fvar,'a+')
else:
    fio = open(fvar,'w')

odbName = sys.argv[2]+'.odb'
odbpath = datapath + odbName

if os.path.exists(odbpath):
    print "We've found the odb!"
else:
    print "Can\'t find the odb!"
    sys.exit()

elset = elemset = None
""" Open the output database """
odb = openOdb(odbName)
assembly = odb.rootAssembly

""" Check to see if the element set exists in the assembly
"""

if elsetName1:
    try:
        elemset1 = assembly.instances[elsetName1]
    except KeyError:
        print 'An assembly level instances named %s does' \
        'not exist in the output database %s' \
        % (elsetName1, odbName)
        odb.close()
        exit(0)

if elsetName2:
    try:
        elemset2 = assembly.instances[elsetName2]
    except KeyError:
        print 'An assembly level instances named %s does' \
        'not exist in the output database %s' \
        % (elsetName2, odbName)
        odb.close()
        exit(0)

s = odb.steps.keys()[-1]
laststep = odb.steps[s]
frm=laststep.frames
frmlast=frm[len(frm)-1]
allFields=frmlast.fieldOutputs


if (allFields.has_key(var)):
    varSet = allFields[var]
    if elemset1:
        varvalue = varSet.getSubset(region=elemset1).values
        vardata1 = []
        for varvalue2 in varvalue:
            vardata1.append(varvalue2.data)
        varmax1 = max(vardata1)
    if elemset2:
        varvalue = varSet.getSubset(region=elemset2).values
        vardata2 = []
        for varvalue2 in varvalue:
            vardata2.append(varvalue2.data)
        varmax2 = max(vardata2)
    fio.write(str(max(varmax1,varmax2))+'\n')
    
fio.close()
odb.close()
