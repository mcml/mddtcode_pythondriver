import math
import os
import sys
import numpy

jobname = sys.argv[1]
basename = sys.argv[2]

try:
    gradual = sys.argv[3]
except:
    gradual = 100




fcycs = basename + 'cycles.dat'
dmax = 0.050
dmean = 0.010
max_step = 1000000
max_cycs = 2000000
# First intermediate cycle is 50000 cycles

if gradual > 99:
    intcycs = [2,25,50,75,gradual,gradual+1]
elif gradual > 50:
    intcycs = [2,20,40,50,gradual,gradual+1]
elif gradual > 25:
    intcycs = [2,3,10,20,gradual,gradual+1]
elif gradual > 10:
    intcycs = [2,5,10,gradual,gradual+1]
elif gradual > 3:
    intcycs = [2,3,gradual,gradual+1]
else:
    incycs = [2,3,4]


intcycs.extend([50000,100000,150000,200000,250000,300000,500000,1000000,1500000,2000000])
mean = True

# Read OutD
#fname = jobname + 'OutD.dat'
#fin = open(fname)
#outdata = fin.read()
#fin.close()

# Read DelD
fname = jobname + 'DelD.dat'
fin = open(fname)
data = fin.read()
fin.close()


data = data.split()
delds = numpy.array([float(num) for num in data])
#outdata = outdata.split()
#outds = numpy.array([float(num) for num in data])

npt = len(delds)
#npt = len(outds)
#stepds = delds + outds

sumd = sum(delds)
#for i in range(npt):
#  if stepds[i] > 0.9:
#    sumd -= delds[i]

# Calculate the mean over all elements that actually are damaging
deld_con_mean = sumd/float(npt)
sumdel = 0.
n = 0
for pt in delds:
    if (pt < 0.99):
        sumdel += pt
        n += 1
if n == 0:
  deld_con_mean2 = 0.
else:
  deld_con_mean2 = sumdel/n

deld_con_max = delds.max()

# Read in the cycles file
if os.path.exists(fcycs):
    fio = open(fcycs,'r+')
    data = fio.readlines()
    last = int(data[-1])
else:
    fio = open(fcycs,'w')
    last = 1
    fio.write(str(last)+'\n') # This script starts on the first micro step, but
                              # one load cycle already occured in fat_inelas0

# Determine the next intermediate cycle to stop at
for i in range(len(intcycs)):
  if last < intcycs[i]:
    intcyc = intcycs[i]
    break

if deld_con_max == 0.0:
    deln = intcyc - last
else:
    meansteps = int(math.floor(dmean/deld_con_mean))
    intsteps = intcyc - last
    deln = min(max_step,meansteps,intsteps)
    #deln = min(int(math.floor(dmax/deld_con_max)),max_step,\
               #int(math.floor(dmean/deld_con_mean)),max_cycs-last)

deln = max(deln,1)

if last > 500000:
  deln = max(deln,2000)


print '--- JUMP ',deln,' cycles ----'
print '--- max damage : ',deld_con_max,' ---'
print '--- mean damage: ',deld_con_mean,' ---'
print '--- mean damage 2: ',deld_con_mean2,' ---'
delds = float(deln)*delds
fout = open(fname,'w')
print 'fout_fname',fname

for deld in delds:
    fout.write(str(deld)+'\n')
fout.close()

fio.write(str(last+deln)+'\n')
fio.close()
