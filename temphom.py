from temphom_func import *
import os,sys,re

try:
    jn = sys.argv[1]
except:
    print 'You need to give me a jobname at least'


ji = job_info(jn)

if ji.make:
    ji.make_umat()

os.chdir(ji.workdir)

# Determine the max stress
#ji.get_maxstress()
#if starting from initial point, startn = 1
#if restarted, startn = n(last) + 1
if ji.restart:
    ji.cleanupoldjob()
    #pass
else:
    print 'Start from initial point'
    ji.cleanupoldjob()
    #ji.write_stress_initial()
    ji.initial_step()
    ji.check_continue(0)
    #ji.rewrite_disp()

for n in range(ji.startn,ji.n_cycs):
    ji.micro_step(n)
    ji.jump(n)
    #ji.rewrite_disp()
    ji.macro_step(n)
    ji.check_continue(n)
    if ji.end:
      break

