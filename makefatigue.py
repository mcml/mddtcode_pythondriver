#!/usr/bin/env python
""" Take abaqus job files (.inp and .ic) and make the fatigue files for them.
"""
import sys,os,re
from fix_els_in_ic import *

jobname = sys.argv[1]
#only remain the jobname and delete things like '.inp' if input 'case.inp'
basename = re.sub('_inelas','',jobname)
basename = re.sub('.inp','',jobname)

print basename

basefile = open(basename+'.inp').read()

# Check that the load is applied as a pressure load
#if re.findall('\*Dsload',basefile):
#    print 'Good, the load is applied as a pressure'
#else:
#    print 'Change the load application to a pressure load!'
#    sys.exit()

# Check that the amplitude is specified
#if re.findall('\*Amplitude, name=Amp-1',basefile):
#    print 'Amplitude Found'
#else:
#    print 'Adding in amplitude after assembly......'
#    amplines = '*End Assembly\n'+\
#               '**\n' +\
#               '*Amplitude, name=Amp-1\n' +\
#               '0., 1.0, 0.5, 0.1, 1., 1.0\n' +\
#               '**\n'
#    basefile = re.sub('\*End Assem.*\n',amplines,basefile)

# Make sure the output requests are right
#outlines = '** OUTPUT REQUESTS\n' +\
#    '** \n' +\
#    '*Restart, write, overlay\n' +\
#    '** \n' +\
#    '** FIELD OUTPUT: F-Output-1\n' +\
#    '** \n' +\
#    '*Output, field\n' +\
#    '*Node Output\n' +\
#    'U, RF\n' +\
#    '**\n' +\
#    '*Output, field\n' +\
#    '*Element Output\n' +\
#    'E,S,SDV28,SDV33,SDV38,SDV43\n' +\
#    '**\n' +\
#    '*End Step\n'
#basefile = re.sub('\*\* OUTPUT(.*\n)*',outlines,basefile)

print 'Make sure the right number of elements are specified in the ic file'
fix_els_in_ic(jobname)

baseic = open(jobname+'.ic').read()

# Make sure the baseic file is set to fatigue
#baseic = re.sub('\*monotoni.*\n.*','*monotonic\n0',baseic)


# For the inelas input file, there should be no changes
open(basename+'_inelas.inp','w').write(basefile)
open(basename+'_inelas.ic','w').write(baseic)

# For the inelasre file(macro), remove the mesh information, change step time
inelasre = re.sub('\*Heading(.*\n)*\*\* STEP','*Heading\n*Restart, Read, End '
    'Step\n**\n** STEP',basefile)
#inelasre = re.sub('\*Static.*\n.*','*Static,Stabilize=1e-3\n0.02, 1., 1e-15, 0.02',inelasre)
inelasre = re.sub('\*Static.*\n.*','*Static\n0.02, 1., 1e-15, 0.02',inelasre)
open(basename+'_inelasre.inp','w').write(inelasre)
open(basename+'_inelasre.ic','w').write(baseic)

# For the elasre file(micro), remove the mesh information, change step time
elasre = re.sub('\*Heading(.*\n)*\*\* STEP','*Heading\n*Restart, Read, End '
    'Step\n**\n** STEP',basefile)

#elasre = re.sub('\*Static.*\n.*','*Static,Stabilize=1e-3\n0.02, 1., 1e-15, 0.02',elasre)
# elasre = re.sub('\*Static.*\n.*','*Static\n0.05, 1., 1e-15, 0.05',elasre)
elasre = re.sub('\*Boundary.*','*Boundary,amplitude=Amp-1',elasre)
open(basename+'_elasre.inp','w').write(elasre)
open(basename+'_elasre.ic','w').write(baseic)
