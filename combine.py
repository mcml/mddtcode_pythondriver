import os
import sys
#import tic

def read_file(fname):
    fin = open(fname)
    data = fin.read()
    fin.close()
    nums = data.split()
    nums = [float(num) for num in nums]
    return nums

#tic.tic()

basename = sys.argv[1]

files = os.listdir('.')
cor_files = []
for f in files:
    if f.startswith(basename):#+'_'):
        cor_files.append(f)

com_nums = read_file(cor_files[0])
for f in cor_files[1:]:
    new_nums = read_file(f)
    for i, num in enumerate(new_nums):
        if num != 0:
            com_nums[i] = num

fout = open(basename+'.dat','w')
for num in com_nums:
    fout.write(str(num)+'\n')
fout.close()

#tic.toc()
