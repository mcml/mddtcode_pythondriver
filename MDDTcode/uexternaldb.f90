SUBROUTINE UEXTERNALDB(LOP,LRESTART,TIME,DTIME,JSTEP,KINC)

    USE INP_COMP
    USE DATATYPES
    USE COEFTENS

    implicit none
!    include 'ABA_PARAM.INC'

    CHARACTER FNAME*200
    INTEGER :: LOP,LRESTART,JSTEP,KINC,TEMP
    REAL(KIND=8)::TIME(2),DTIME

    INTEGER :: dummy,IINT,STAT,i


    IF (LOP == 0) THEN              !BEGINNING ANALYSIS

        call getoutdir(outdir, lenoutdir)
        call getjobname(jobname,lenjobname)
        CALL READ_COMP
        CALL READ_COEFTENS_EHM
        CALL ASSIGN_MATERIAL

        write(*,*) 'compread_beginning'

        IF ((mono == 0).and.(multemp == 1)) THEN
          ALLOCATE(OMEGA_DEL(NUM_ELES,NUM_INT_PTS_ELE,NUM_INTFC))
          OMEGA_DEL = 0.d0
          ALLOCATE(OMEGA_HOLD(NUM_ELES,NUM_INT_PTS_ELE,NUM_INTFC))
          OMEGA_HOLD = 0.d0
          ALLOCATE(OMEGA_DATA(NUM_ELES,NUM_INT_PTS_ELE,NUM_INTFC))
          OMEGA_DATA = 0.d0
          ALLOCATE(DELTA_HOLD(NUM_ELES,NUM_INT_PTS_ELE,LEND_ex))
          DELTA_HOLD = 0.d0
          MICRO = 0
        ENDIF

      ELSE IF (LOP == 3) THEN             !END OF ANALYSIS


          ! If it is micro step, write delta_omega to deld.dat
          ! omega_ap = deln * delta_omega/tao_0, where tao_0 = 1
          IF ((MONO == 0).and.(multemp == 1)) THEN
            IF (MICRO == 1) THEN
              write(*,*) 'Micro step - write deld'
              FNAME = TRIM(ADJUSTL(outdir))//'/'//TRIM(ADJUSTL(jobname))//'DelD'//'.dat'
              !write(*,*) 'save OMEGA_DATA and OMEGA HOLD to: ',FNAME
              OPEN(555, FILE=FNAME, ACTION='WRITE', STATUS='REPLACE', IOSTAT=STAT)
              IF (stat .ne. 0) then
                write(*,*) FNAME," Could not be opened for writing"
              ELSE
                DO i = 1,NUM_INTFC
                  if (E_INTFC(i) == 1) then
                    WRITE(555,*) OMEGA_DATA(:,:,i)-OMEGA_HOLD(:,:,i)
                  endif
                enddo
              ENDIF
              CLOSE(555, STATUS='KEEP')
            END IF
          ENDIF

          deallocate(E_INTFC, lmicro, vfmicro, helem,INTFCPARM0,INTFCPROP)
          deallocate(PROPTS,kf,softslope,MT)

          if (ALLOCATED(OMEGA_DATA)) deallocate(OMEGA_DATA)
          if (ALLOCATED(OMEGA_HOLD)) deallocate(OMEGA_HOLD)
          if (ALLOCATED(DELTA_HOLD)) deallocate(DELTA_HOLD)

      ELSE IF (LOP == 4) THEN             !BEGINNING OF RESTART

          call getoutdir(outdir, lenoutdir)
          call getjobname(jobname,lenjobname)
          CALL READ_COMP
          CALL READ_COEFTENS_EHM
          CALL ASSIGN_MATERIAL

          write(*,*) 'compread_restart'

        IF ((MONO == 0).and.(multemp == 1)) THEN
          ALLOCATE(OMEGA_DEL(NUM_ELES,NUM_INT_PTS_ELE,NUM_INTFC))
          OMEGA_DEL = 0.d0
          ALLOCATE(OMEGA_HOLD(NUM_ELES,NUM_INT_PTS_ELE,NUM_INTFC))
          OMEGA_HOLD = 0.d0
          ALLOCATE(OMEGA_DATA(NUM_ELES,NUM_INT_PTS_ELE,NUM_INTFC))
          OMEGA_DATA = 0.d0
          ALLOCATE(DELTA_HOLD(NUM_ELES,NUM_INT_PTS_ELE,LEND_ex))
          DELTA_HOLD = 0.d0

  ! Get whether it is a micro step

          FNAME = TRIM(ADJUSTL(outdir))//'/'//TRIM(ADJUSTL(jobname))//'micmac.dat'
          OPEN(555, FILE = FNAME, ACTION='READ', IOSTAT=STAT)
          if (stat .ne. 0) then
            write(*,*) FNAME," Could not be opened for reading"
            write(*,*) 'Assume that this is a macrostep then'
            MICRO = 0
          else
            READ(555,*) MICRO
          endif
          IF (MICRO == 1) THEN
            KINC_INI = KINC
          ENDIF

          CLOSE(555, STATUS='KEEP')


  ! If it is macro-step, get omega_ap from delD.dat
          IF (MICRO == 0) THEN
            !write(*,*) 'At beginning of restart, not micro'
            FNAME = TRIM(ADJUSTL(outdir))//'/'//TRIM(ADJUSTL(jobname))//'DelD.dat'
            !write(*,*) 'Read OMEGA_DEL from: ',FNAME
            OPEN(555, FILE = FNAME, ACTION='READ', IOSTAT=STAT)
            if (stat .ne. 0) then
              !write(*,*) FNAME," Could not be opened for reading"
              OMEGA_DEL = 0.d0
            else
              DO i = 1,NUM_INTFC
                if (E_INTFC(i) == 1) then
                  READ(555,*) OMEGA_DEL(:,:,i)
                endif
              enddo
              !write(*,*) 'OMEGA_DEL(:,:,2)',OMEGA_DEL(:,:,2)
            endif
            CLOSE(555, STATUS='KEEP')
          END IF

        END IF

      END IF

  !300 write(*,*) 'uexternaldb done'
  !pause
      RETURN


END SUBROUTINE UEXTERNALDB
!**********************************************************************
!**********************************************************************


!**********************************************************************
!**********************************************************************
!
! READING DATA FROM THE INPUT COMPANION (.IC) FILE
!
!**********************************************************************
SUBROUTINE READ_COMP

    USE INP_COMP

    CHARACTER(LEN=100):: line
    INTEGER, PARAMETER :: maxrecs = 1000000
    INTEGER :: ios,nrec,i,j,ROM,STAT,tmp
    LOGICAL :: comp
    CHARACTER(LEN=255) :: CWD,FILENAME
    CHARACTER(LEN=30) tmpLine

    !**********************************************************************
    ! Point it to the .ic file
    !**********************************************************************

    FILENAME = TRIM(adjustl(outdir))//'/'//TRIM(ADJUSTL(jobname))//'.ic'
    write(*,*) TRIM(ADJUSTL(outdir))//'/'//TRIM(ADJUSTL(jobname))//'.ic is read'

    open(100,file=TRIM(ADJUSTL(FILENAME)),action='read')

    do j=1,maxrecs
        read(100,*,iostat=ios) line
        if (ios /= 0) exit
        tmpLine = trim(adjustl(line))

        select case (tmpLine)
            case( '*nintfc')
                read(100,*) NUM_INTFC
            case( '*noel' )
                read(100,*) NUM_ELES
            case( '*npt' )
                read(100,*) NUM_INT_PTS_ELE

            case( '*mono' )
                read(100,*) mono

            case( '*multitemperal' )
                read(100,*) multemp

            case( '*qs' )
                read(100,*) QS

            case( '*neintfc' )
                if (allocated(E_INTFC)) deallocate(E_INTFC)
                allocate(E_INTFC(NUM_INTFC))
                read(100,*) E_INTFC(1:NUM_INTFC)

            case( '*l_micro' )
                if (allocated(lmicro)) deallocate(lmicro)
                allocate(lmicro(NUM_INTFC))
                read(100,*) lmicro(1:NUM_INTFC)

            case( '*vf_micro' )
                if (allocated(vfmicro)) deallocate(vfmicro)
                allocate(vfmicro(NUM_INTFC))
                read(100,*) vfmicro(1:NUM_INTFC)

            case( '*h_elem' )
                if (allocated(helem)) deallocate(helem)
                allocate(helem(NUM_INTFC))
                read(100,*) helem(1:NUM_INTFC)

            case( '*kf' )
                if (allocated(kf)) deallocate(kf)
                allocate(kf(NUM_INTFC,3))
                do i = 1,NUM_INTFC
                    read(100,*) kf(i,1:3)
                end do
            case( '*softslope' )
                if (allocated(softslope)) deallocate(softslope)
                allocate(softslope(NUM_INTFC,2))
                do i = 1,NUM_INTFC
                    read(100,*) softslope(i,1:2)
                end do

            case( '*cparm' )
                read(100,*) c_parm
            case( '*penparm' )
                read(100,*) pen_parm

            case( '*IntfcParm' )
                if (allocated(INTFCPARM0)) deallocate(INTFCPARM0)
                allocate(INTFCPARM0(6,NUM_INTFC))
                do i = 1,NUM_INTFC
                    read(100,*) INTFCPARM0(1:6,i)
                end do

            case( '*coeftens' )
                read(100,*) N_PROPTS
                if (.not. allocated(propts)) allocate(PROPTS(N_PROPTS))
                read(100,*) PROPTS
        end select
        nrec = nrec + 1
    end do
    close(100)

    RETURN

END SUBROUTINE READ_COMP
!**********************************************************************
!**********************************************************************

!**********************************************************************
!**********************************************************************
!
! READING, COMPUTING AND STORING ALL MATERIAL PAREMETERS
!
!**********************************************************************
SUBROUTINE ASSIGN_MATERIAL

   USE INP_COMP
   USE COEFTENS
   USE DATATYPES

   Implicit None

   INTEGER :: IINT
   INTEGER, PARAMETER :: NTENS=6

if (.not. allocated(intfcprop)) allocate(intfcprop(num_intfc))

     DO IINT = 1,NUM_INTFC
         INTFCPROP(IINT)%a1Int = INTFCPARM0(1,IINT)
         INTFCPROP(IINT)%a2Int = INTFCPARM0(2,IINT)
         INTFCPROP(IINT)%ckInt(1:2) = INTFCPARM0(3:4,IINT)
         INTFCPROP(IINT)%ckInt(3) = INTFCPROP(IINT)%ckInt(2)
         INTFCPROP(IINT)%eqvStnIniInt = INTFCPARM0(5,IINT)
         INTFCPROP(IINT)%gamma = INTFCPARM0(6,IINT)
     END DO

END SUBROUTINE ASSIGN_MATERIAL
!*******************************************************************************
!*******************************************************************************


!**********************************************************************
!
! ALLOCATING AND STORING COEFFICIENT TENSORS
!
!**********************************************************************
SUBROUTINE READ_COEFTENS_EHM

    USE INP_COMP
    USE COEFTENS
    USE DATATYPES

    implicit none

    INTEGER :: NTENS
    INTEGER :: ICTR, I, K1, K2, J, IPH1, I1,K
    LOGICAL :: COMP

    NTENS = 6
    MTDIM = 6
    NSD_ex = 3

    IF (.NOT. ALLOCATED(MT)) ALLOCATE ( MT( MTDIM ) )

    MT = (/ 1, 2, 3, 4, 5, 6 /)

    NUMINT_ex = INT(PROPTS(1))
    NPH_ex = INT(PROPTS(2))

!f    write(*,"(A,I5.2, A, I5.2)")' NUMINT: ', INT(PROPTS(1)), ' NPH: ', INT(PROPTS(2))
    LEND_ex = NTENS * NPH_ex + NSD_ex * NUMINT_ex
!    LEND2 = MTDIM * NPH + NSD * NUMINT
    NUMINT2_ex = NSD_ex* NUMINT_ex
    !SET THE APPROPRIATE COEFFICIENT TENSORS

    CALL ALLOC_COEFTENS_EHM(CF_T,ICTR,NTENS,NUMINT_ex, NPH_ex, NSD_ex, NUMINT2_ex)
    ICTR = 2
    CALL SET_COEFTENS_EHM(CF_T,ICTR,NTENS,MTDIM, NUMINT_ex, MT, NPH_ex, NSD_ex, NUMINT2_ex, PROPTS, N_PROPTS)
    !Calculate Coefficient Tensor D and R when EPSILON_BAR is considered as the forcing term
    !CALL NEWCT(LL, NTENS, CT)

    !Coefficient tensor scaling
    CALL ELEMSCALING(NTENS, CF_T)
    RETURN

END SUBROUTINE READ_COEFTENS_EHM
!**********************************************************************
!**********************************************************************


!**************************************************************************************
!**************************************************************************************
SUBROUTINE ALLOC_COEFTENS_EHM(CF,ICTR,NTENS,NUMINT, NPH, NSD, NUMINT2)

  ! Subroutine that allocates the coefficient tensors
  ! This get it ready for SET_COEFTENS


  USE DATATYPES

  IMPLICIT NONE

  TYPE(CoefTensStr) :: CF
  INTEGER, INTENT(IN) :: NTENS, NUMINT, NPH, NSD, NUMINT2
  INTEGER, INTENT(INOUT) :: ICTR

  IF (.NOT. ALLOCATED(CF%STIFF)) THEN
    ALLOCATE (CF%STIFF(NTENS, NTENS), CF%MBAR(NPH, NTENS, NTENS ))
  	ALLOCATE (CF%STIFFA(NPH, NTENS, NTENS), CF%STIFFM( NPH, NTENS, NTENS), CF%COEFP(NPH,NPH,NTENS,NTENS))
  	ALLOCATE (CF%RBAR(NSD,NUMINT,NTENS))
  	ALLOCATE (CF%RR(NSD, NPH,NUMINT,NTENS), CF%CC(NSD, NUMINT,NTENS))
  	ALLOCATE (CF%FF(NSD, NUMINT,NPH,NTENS), CF%DE( NUMINT2, NUMINT2))
  END IF


END SUBROUTINE ALLOC_COEFTENS_EHM

!***********************************************************************
!***********************************************************************


SUBROUTINE SET_COEFTENS_EHM(CF,ICTR,NTENS,MTDIM, NUMINT, MT, &
     NPH, NSD,NUMINT2, PROPTS, NPROPTS)

  ! Subroutine that sets the values of the coefficient tensors from the
  ! property arrays read from the .ic file.

  USE DATATYPES

  IMPLICIT NONE

  TYPE(CoefTensStr) :: CF
  INTEGER, INTENT(IN) :: NTENS,MTDIM, NUMINT, NPH, NSD, NPROPTS, NUMINT2
  INTEGER, INTENT(INOUT) :: ICTR
  INTEGER, INTENT(IN) :: MT(MTDIM)
  REAL (KIND=8), INTENT(IN) :: PROPTS(NPROPTS)

  INTEGER :: K1, K2, I, J, K, IPH1, I1

  CF%STIFF = 0.d0
  DO I = 1, MTDIM
     CF%STIFF( MT(I) , MT(I) ) = PROPTS(I+2)
  END DO
  ICTR = 1 + I

  DO K1 = 1, MTDIM - 1
     DO K2 = k1+1, MTDIM
        ICTR = ICTR + 1
        CF%STIFF(MT(K2),MT(K1)) = PROPTS(ICTR)
        CF%STIFF(MT(K1),MT(K2)) = PROPTS(ICTR)
     END DO
  END DO

  CF%MBAR = 0.d0
  DO I = 1, NPH
     DO K1 = 1, MTDIM
        DO K2 = 1, MTDIM
           ICTR = ICTR + 1
           CF%MBAR(I,MT(K2),MT(K1)) = PROPTS(ICTR)
        END DO
     END DO
  END DO

  CF%RBAR = 0.d0
  DO I = 1, NSD
     DO J = 1, NUMINT
        DO K1 = 1, MTDIM
           ICTR = ICTR + 1
           CF%RBAR(I,J,MT(K1)) = PROPTS(ICTR)
        END DO
     END DO
  END DO


  CF%STIFFA = 0.d0
  DO I = 1, NPH
     DO K1 = 1, MTDIM
        DO K2 = 1, MTDIM
           ICTR = ICTR + 1
           CF%STIFFA(I,MT(K2),MT(K1)) = PROPTS(ICTR)
        END DO
     END DO
  END DO

  CF%STIFFM = 0.d0
  DO I = 1, NPH
     DO K1 = 1, MTDIM
        DO K2 = 1, MTDIM
           ICTR = ICTR + 1
           CF%STIFFM(I,MT(K2),MT(K1)) = PROPTS(ICTR)
        END DO
     END DO
  END DO

  CF%COEFP = 0.d0
  DO I = 1, NPH
     DO J = 1, NPH
        DO K1 = 1, MTDIM
           DO K2 = 1, MTDIM
              ICTR = ICTR + 1
              CF%COEFP(I,J,MT(K2),MT(K1)) = PROPTS(ICTR)
           END DO
        END DO
     END DO
  END DO

  CF%RR = 0.d0
  DO I1 = 1, NSD
     DO I = 1, NPH
        DO J = 1, NUMINT
           DO K1 = 1, MTDIM
              ICTR = ICTR + 1
              CF%RR(I1,I,J,MT(K1)) = PROPTS(ICTR)
           END DO
        END DO
     END DO
  END DO

  CF%CC = 0.d0
  DO I1 = 1, NSD
     DO J = 1, NUMINT
        DO K1 = 1, MTDIM
           ICTR = ICTR + 1
           CF%CC(I1,J,MT(K1)) = PROPTS(ICTR)
        END DO
     END DO
  END DO

  CF%FF = 0.d0
  DO I1 = 1, NSD
     DO I = 1, NPH
        DO J = 1, NUMINT
           DO K1 = 1, MTDIM
              ICTR = ICTR + 1
              CF%FF(I1,I,J,MT(K1)) = PROPTS(ICTR)
           END DO
        END DO
     END DO
  END DO

  DO I = 1, NUMINT2
     CF%DE(:,I) = PROPTS(ICTR+1 : ICTR+ NUMINT2 )
     ICTR = ICTR + NUMINT2
  END DO

  RETURN

END SUBROUTINE SET_COEFTENS_EHM

SUBROUTINE ELEMSCALING(NTENS, CT)

  USE INP_COMP
  USE COEFTENS
  USE DATATYPES

  INCLUDE 'aba_param.inc'

  INTEGER :: IINT,I,J,K,NTENS
  REAL(KIND=8) :: zeta(NUMINT_ex), eta_N(NUMINT_ex), eta_T1(NUMINT_ex), eta_T2(NUMINT_ex)
  REAL(KIND=8) :: C_111,C_122,C_133,C_211,C_222,C_233,C_311,C_322,C_333
  REAL(KIND=8) :: C_N,E_N,D_N,D_21,D_31,nu_1,nu_2,miu_1,miu_2

  ! Coefficient tensor structure
  TYPE(CoefTensStr) :: CT

  DO I = 1, NUMINT_ex
    zeta(i) = helem(I)/(lmicro(I)/vfmicro(I))   ! delamination
  ENDDO

!===================================
!matrix cracking path

!C_ijk are the component of C tensor expressed in local coordinates
iint = 2
C_111 = CT%CC(1,2,1)
C_122 = CT%CC(1,2,2)
C_133 = CT%CC(1,2,3)
C_211 = CT%CC(2,2,1)
C_222 = CT%CC(2,2,2)
C_233 = CT%CC(2,2,3)
C_311 = CT%CC(3,2,1)
C_322 = CT%CC(3,2,2)
C_333 = CT%CC(3,2,3)
D_21 = CT%DE(5,2)
D_31 = CT%DE(8,2)

!nu and miu are not incorporated right now because the minor components of C and D are not accurate
nu_1 =0.d0!-(C_211*C_333 - C_311*C_233)/(C_222*C_333 - C_233*C_322)
nu_2 =0.d0!-(C_211*C_322 - C_311*C_222)/(C_233*C_322 - C_333*C_222)
miu_1 =0.d0!-(D_21*C_333 - D_31*C_233)/(C_222*C_333 - C_233*C_322)
miu_2 =0.d0!-(D_21*C_322 - D_31*C_222)/(C_233*C_322 - C_333*C_222)

C_N = C_111 + C_122 * nu_1 + C_133 * nu_2
D_N = CT%DE(2,2) + C_122 * miu_1 + C_133 * miu_2
E_N = CT%Stiff(1,1) + CT%Stiff(1,2) * nu_1 + CT%Stiff(1,3) * nu_2
Z_N = CT%RBAR(2,1,1) + CT%Stiff(1,2) * miu_1 + CT%Stiff(1,3) * miu_2

!================Residual stiffness restraints

CT%RBAR(iint,1,1) = (1-kf(2,1)) * E_N*D_N / C_N - CT%Stiff(1,2) * miu_1 - CT%Stiff(1,3) * miu_2
CT%RBAR(iint,2,4) = (1-kf(2,2)) * CT%Stiff(4,4)*CT%DE(5,5) / CT%CC(2,2,4)
CT%RBAR(iint,3,5) = (1-kf(2,3)) * CT%Stiff(5,5)*CT%DE(8,8) / CT%CC(3,2,5)

!================scaling softening stiffness

eta_N(2) = softSlope(2,1) / ( zeta(2)*softSlope(2,1) +kf(2,1)*(zeta(2)-1)*D_N)
eta_T1(2) = softSlope(2,2) / ( zeta(2)*softSlope(2,2) +kf(2,2)*(zeta(2)-1)*CT%DE(5,5))
eta_T2(2) = softSlope(2,2) / ( zeta(2)*softSlope(2,2) +kf(2,3)*(zeta(2)-1)*CT%DE(8,8))

CT%DE(:,2) = CT%DE(:,2) * eta_N(2)
CT%DE(:,5) = CT%DE(:,5) * eta_T1(2)
CT%DE(:,8) = CT%DE(:,8) * eta_T2(2)

CT%RBAR(iint,1,:) = CT%RBAR(iint,1,:) * eta_N(2)
CT%RBAR(iint,2,:) = CT%RBAR(iint,2,:) * eta_T1(2)
CT%RBAR(iint,3,:) = CT%RBAR(iint,3,:) * eta_T2(2)

! CT%RBAR(iint,1,1) = CT%RBAR(iint,1,1) * eta_N(2)
! CT%RBAR(iint,1,2) = CT%RBAR(iint,1,2) * 1/zeta(2)!Z221 and Z331 should be scaled by xi when residual corrections are applied
! CT%RBAR(iint,1,3) = CT%RBAR(iint,1,3) * 1/zeta(2)!(verified by unit box examples)
! CT%RBAR(iint,2,4) = CT%RBAR(iint,2,4) * eta_T1(2)
! CT%RBAR(iint,3,5) = CT%RBAR(iint,3,5) * eta_T2(3)

!fibercracking path

C_111 = CT%CC(1,3,3)
C_122 = CT%CC(1,3,1)
C_133 = CT%CC(1,3,2)
C_211 = CT%CC(2,3,3)
C_222 = CT%CC(2,3,1)
C_233 = CT%CC(2,3,2)
C_311 = CT%CC(3,3,3)
C_322 = CT%CC(3,3,1)
C_333 = CT%CC(3,3,2)
D_21 = CT%DE(6,3)
D_31 = CT%DE(9,3)
nu_1 =0.d0!-(C_211*C_333 - C_311*C_233)/(C_222*C_333 - C_233*C_322)
nu_2 =0.d0!-(C_211*C_322 - C_311*C_222)/(C_233*C_322 - C_333*C_222)
miu_1 =0.d0!-(D_21*C_333 - D_31*C_233)/(C_222*C_333 - C_233*C_322)
miu_2 =0.d0!-(D_21*C_322 - D_31*C_222)/(C_233*C_322 - C_333*C_222)

C_N = C_111 + C_122 * nu_1 + C_133 * nu_2
D_N = CT%DE(3,3) + C_122 * miu_1 + C_133 * miu_2
E_N = CT%Stiff(3,3) + CT%Stiff(3,1) * nu_1 + CT%Stiff(3,2) * nu_2
Z_N = CT%RBAR(3,1,3) + CT%Stiff(3,1) * miu_1 + CT%Stiff(3,2) * miu_2

iint = 3
!================Residual stiffness restraints

CT%RBAR(iint,1,3) = (1-kf(3,1)) * E_N*D_N /C_N - CT%Stiff(3,1) * miu_1 - CT%Stiff(3,2) * miu_2
CT%RBAR(iint,2,5) = (1-kf(3,2)) * CT%Stiff(5,5) * CT%DE(6,6) / CT%CC(2,3,5)
CT%RBAR(iint,3,6) = (1-kf(3,3)) * CT%Stiff(6,6) * CT%DE(9,9) / CT%CC(3,3,6)

!================scaling softening stiffness

eta_N(3) =softSlope(3,1) / ( zeta(3)*softSlope(3,1) + kf(3,1)*(zeta(3)-1)*D_N)
eta_T1(3) = softSlope(3,2) / ( zeta(3)*softSlope(3,2) +kf(3,2)*(zeta(3)-1)*CT%DE(6,6))
eta_T2(3) = softSlope(3,2) / ( zeta(3)*softSlope(3,2) +kf(3,3)*(zeta(3)-1)*CT%DE(9,9))

CT%DE(:,3) = CT%DE(:,3) * eta_N(3)
CT%DE(:,6) = CT%DE(:,6) * eta_T1(3)
CT%DE(:,9) = CT%DE(:,9) * eta_T2(3)

CT%RBAR(iint,1,:) = CT%RBAR(iint,1,:) * eta_N(3)
CT%RBAR(iint,2,:) = CT%RBAR(iint,2,:) * eta_T1(3)
CT%RBAR(iint,3,:) = CT%RBAR(iint,3,:) * eta_T2(3)


!write(7,*) '==============during scaling======================'
!write(7,*) 'eta_N'
!write(7,*) eta_N(2),eta_N(3)
!write(7,*) 'eta_T1'
!write(7,*) eta_T1(2),eta_T1(3)
!write(7,*) 'eta_T2'
!write(7,*) eta_T2(2),eta_T2(3)
!write(7,*) '===================================='

END SUBROUTINE
