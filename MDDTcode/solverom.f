SUBROUTINE ROM(STRAN, DSTRAN, STRESS, FJAC1, FJAC2, STVIN, STVOUT, NTENS, NDI, NSTATV, NUMINT2, NSD, NumInt, lenD, KINC, DTIME, NOEL, NPT, INTPR, PNEWDT, CT, flag, noel_check, cparm, penparm, OMEGA_AP, OMEGA_INI, DELTA_INI, mono_t, QS_t, multemp_t, micro_t, E_INTFC_t)

  !USE INP_COMP
  !USE COEFTENS
  USE DATATYPES
  USE CALL_TERMINATION
  !USE KPARM
  !USE KVARM

  Implicit None

  REAL(KIND=8) :: STRAN(NTENS), STRAN_OLD(NTENS), STRAN_BAR(NTENS), DSTRAN(NTENS), Stress(NTENS), StvIn(NSTATV), StvOut(NSTATV)
  REAL(KIND=8), INTENT(IN) :: DTIME
  REAL(KIND=8) :: FJAC1(lenD,lenD), FJAC2(lenD,NTENS)
  REAL(KIND=8) :: RHS(LEND), NN(NUMINT),ll(NUMINT),hh(NUMINT)
  REAL(KIND=8) :: R_ALPHA(nsd,nsd), D_ALPHA(nsd), L_N_ALPHA(nsd,nsd), L_N_ALPHA_VOIGT(ntens)
  REAL(KIND=8) :: D(lenD),dOld(lenD),dmgInt(NumInt),dmgIntOld(NUMINT),dmgIntOld_pre(NUMINT)
  REAL(KIND=8) :: rdisp(NumInt2),rdispOld(NumInt2), rdisp_temp(NumInt2),DELTA_INI_temp(NumInt2),trac(NumInt2),ckInt(nsd,numint)
  REAL(KIND=8) :: phi(numint), phi_old(numint), phi_old_pre(numint), PNEWDT
  INTEGER :: NTENS, NDI, NSTATV, NumInt2, noel, npt, flag, KINC
  INTEGER :: i, j, ictr, i1, j1, ii, ALPHA, NSD, NumInt, lenD, E_INTFC_t(NumInt)
  LOGICAL:: check
  TYPE(INTFCPARM) :: INTPR(numint)
  TYPE(CoefTensStr) :: CT
  INTEGER :: noel_check, mono_t, QS_t, multemp_t, micro_t
  !for passing variables from uexternaldb
  REAL(KIND=8) :: OMEGA_AP(NumInt), OMEGA_INI(NumInt), DELTA_INI(NumInt2), cparm, penparm

! *********************************************************************
! Step (1) : Compute Current Homogenized Strain Tensor
! *********************************************************************

  stran_old = stran
  stran = stran + dstran

! *********************************************************************
! Step (2) : Compute constant part of the strain induced force (rhs)
!            and phase average strain
! *********************************************************************

  i1 = 0

  DO ii = 1, nsd
     DO i = 1, numint
        rhs( i1+i+(ii-1)*numint ) = - DOT_PRODUCT( CT%cc(ii,i,:), stran)
     END DO
  END DO

! *********************************************************************
! Step (3) : Initialize the Newton algorithm
! *********************************************************************

  rdispOld = (/ StvIn( 1 : lenD ) /)
  dOld = (/ StvIn( 1 : lenD ) /)
  ictr =lenD
  dmgIntOld = (/ StvIn(ictr+1 : ictr + numint) /)

  if (mono_t == 0) then
    ictr =ictr + numint + lenD
    phi_Old = (/ StvIn( ictr+1 : ictr + numint) /)
  endif

! Record damage(omega) and separation(delta) at the first increment step

  d = dOld
  rdisp = rdispOld

  IF (noel.eq.noel_check) THEN
    WRITE(7,*) '+++++++++++++++++++++++'
    WRITE(7,*) 'noel',noel
    WRITE(7,*) 'stran',stran
    WRITE(7,*) '+++++++++++++++++++++++'
  END IF

!when completely damaged at every failure cracking direction,
!the element become completely elastic
  IF ((dmgIntOld(2).eq.1.d0).and.(dmgIntOld(3).eq.1.d0) ) THEN
    dmgInt = dmgIntOld
    stress = 5.d-5/1.d0 * MATMUL(CT%stiff, stran)
    trac = 0.d0
    DO i = 1, numint
      trac(i) = ( 1.d0 - dmgInt(i) ) * IntPr(i)%ckInt(1) * rdisp(i) + DMIN1( rdisp(i), 0.d0 ) / penParm
      DO j = 2,nsd
        trac(i+(j-1)*numint) = ( 1.d0 - dmgInt(i) ) * IntPr(i)%ckInt(j) * rdisp(i+(j-1)*numint)
      END DO
    END DO

  ELSE

    !IF (dtime.gt.1.d-4) THEN
      CALL KNEWT(D, dold, fjac1, dmgInt, dmgIntOld,  dmgIntOld_pre, phi, phi_old, phi_old_pre, RHS,&
                 INTPR, CT, NTENS, NDI, NSTATV, NUMINT2, NSD, NumInt, lenD, DTIME, NOEL, NPT, noel_check, &
                 cparm, penparm, OMEGA_AP, mono_t, multemp_t, micro_t, E_INTFC_t, check)
      flag = 0
    !ELSE
      !CALL EXPLICIT(D, dold, fjac2, dmgInt, dmgIntOld, stran_old, dstran, INTPR, CT, NTENS)
      !flag = 1
      !check = .false.
      !write(*,*) 'call explicit'
      !write(7,*) 'call explicit'
    !END IF

    IF (check) THEN
      IF (dtime.gt.1.d-17) THEN
        WRITE(*,*) 'noel',noel
        WRITE(7,*) 'noel',noel
        WRITE(*,*) 'Requesting step cutback'
        WRITE(7,*) 'Requesting step cutback'
        pnewdt = 5.d-1
        !CALL XIT
        !WRITE(7,*) 'stran',stran

        !termination = .true.
      ELSE
        !WRITE(*,*) 'Minimum Step Size Allowed has been reached.'
        WRITE(7,*) 'Minimum Step Size Allowed has been reached.'
        WRITE(7,*) 'noel',noel
        !WRITE(7,*) 'stran',stran
        termination = .true.
      END IF
    END IF

    ! Updated displacement jumps
    rdisp = (/ d( 1 : lenD ) /)

    ! Calculate traction: t = (1-omega)*k*delta, with penalty method
    trac = 0.d0

    DO i = 1, numint
      trac(i) = ( 1.d0 - dmgInt(i) ) * IntPr(i)%ckInt(1) * rdisp(i) + DMIN1( rdisp(i), 0.d0 ) / penParm
      DO j = 2,nsd
        trac(i+(j-1)*numint) = ( 1.d0 - dmgInt(i) ) * IntPr(i)%ckInt(j) * rdisp(i+(j-1)*numint)
      END DO
    END DO

!    if (dmgInt(2).eq.1.d0) then
!      rdisp_temp(5) = 0.d0
!      rdisp_temp(8) = 0.d0
!    end if

!  calculate stress: sigma = L : epsilon + R . delta
    stress = MATMUL(CT%stiff, stran)

    IF ((mono_t == 0).and.(multemp_t == 1).and.(QS_t == 1).and.(MICRO_t == 1)) THEN
    !when fiber completely damaged, no contribution of tangential separation to stress
      DELTA_INI_temp = DELTA_INI
      IF (dmgInt(3).eq.1.d0) then
        DELTA_INI_temp(6) = 0.d0
        DELTA_INI_temp(9) = 0.d0
      END IF

      DO i=1,numint
        DO j = 1,nsd
          stress = stress + CT%RBar(i,j,:) * DELTA_INI_temp( (j-1)*numint + i )
        END DO
      END DO

    ELSE
    !when fiber completely damaged, no contribution of tangential separation to stress
      rdisp_temp = rdisp
      if (dmgInt(3).eq.1.d0) then
        rdisp_temp(6) = 0.d0
        rdisp_temp(9) = 0.d0
      end if

      DO i=1,numint
        DO j = 1,nsd
          stress = stress + CT%RBar(i,j,:) * rdisp_temp( (j-1)*numint + i )
        END DO
      END DO

    ENDIF

    IF (noel.eq.noel_check) THEN
      WRITE(7,*) '+++++++++++++++++++++++'
      WRITE(7,*) 'noel',noel
      WRITE(7,*) 'stress',stress
      WRITE(7,*) '+++++++++++++++++++++++'
    END IF

!when completely damaged, normal residual stiffness => small elasticity
    if (dmgInt(2).eq.1.d0) then
       stress(1) = 5.d-5/1.d0 * CT%stiff(1,1)*stran(1)
    end if

    if (dmgInt(3).eq.1.d0) then
       stress(3) = 5.d-5/1.d0 * CT%stiff(3,3)*stran(3)
    end if

  END IF

! ***********************************************
!  Step (7):Update the state variables
! ***********************************************
  !1-9
  StvOut(1 : lenD) = d
  ictr =lenD
  !10-12
  StvOut(ictr+1 : ictr + numint) = dmgInt
  ictr = ictr + numint
  !13-21
  StvOut(ictr+1 : ictr + lenD) = trac
  ictr = ictr + lenD

  IF (mono_t == 0) THEN
    !22-24
    StvOut(ictr+1 : ictr + numint) = phi
    ictr = ictr + numint
  ENDIF


10 format( 4(E12.5,2x) )
20 format( 100(E25.18,2x) )
25 format( 100(E17.10,2x) )
30 format( 8(E12.5,2x) )

 Return

END SUBROUTINE ROM
!**************************************************************************************
!**************************************************************************************



!**********************************************************************************************************
!
!  THIS SUBROUTINE COMPUTES THE UPDATED DDSDDE
!
!**********************************************************************************************************
SUBROUTINE DDSDDE_UPDATE(STRAN, DSTRAN, FJAC1, FJAC2, DTEMP, NTENS, DDSDDE, STATEV, NSTATV, NSTATV_PH, NUMINT2, NSD, NumInt, lenD, NOEL, CT, flag)

  !USE COEFTENS
  USE DATATYPES
  USE KNR

  Implicit none

  REAL(KIND=8) :: STRAN(NTENS), DSTRAN(NTENS), DTEMP, STRESS(NTENS), DDSDDE(NTENS,NTENS)
  REAL(KIND=8) :: Stiff1(NSD, NUMINT, NTENS), STATEV(NSTATV), tmpVec(NTENS)
  REAL(KIND=8) :: fjac1(lenD,lenD), fjac2(lenD, NTENS), dddstn(lenD, ntens)
  REAL(KIND=8) :: rdisp(NumInt2)
  INTEGER :: indx(lenD)
  INTEGER :: I, J, I1, J1, K1, K2, ictr, II, flag
  INTEGER :: NTENS, NumInt2, NSTATV, NStatv_Ph, NStatv_Int,NOEL, NSD, NumInt, lenD
  TYPE(CoefTensStr) :: CT

!**********************************************************************************
! Step (7): Consistent linearized moduli: partly finite difference approximation
!**********************************************************************************

  IF (flag.eq.0.d0) THEN
  ! calculate ddsdde when newton-raphson method is used
    dddstn = 0.d0
    i1 = 0

    DO ii = 1, nsd
      DO i = 1, numint
        dddstn( i1+i+(ii-1)*numint,: ) = - CT%cc(ii,i,:)
      END DO
    END DO

 ! (7d) Derivative of K with respect to state variables multiplied by
 !      state variables is in ckDerD

    CALL KLUDCMP(fjac1,indx,dTemp)       !Solve linear equations by LU decomposition.
    DO i  =1, ntens
       CALL KLUBKSB(fjac1,indx,dddstn(:,i))
    END DO

    ddsdde = CT%Stiff

    stiff1 = 0.d0
    DO i = 1, numint
      DO j = 1, nsd
        ii = (j-1) * numint + i
        DO k1 = 1, ntens
          stiff1(j,i,k1) = dddstn(ii, k1)
        END DO
      END DO

!when fiber completely damaged, no contribution of tangential separation to stress
      IF (STATEV(lenD + 3) .EQ. 1.d0) THEN
        stiff1(2:3,3,:) = 0.d0
      END IF

    END DO

    DO i = 1, numint
       DO j = 1, nsd
          DO k1 = 1, ntens
             DO k2 = 1, ntens
                ddsdde(k1,k2) = ddsdde(k1,k2) + CT%RBar(i,j,k1) * stiff1(j,i,k2)
             END DO
          END DO
       END DO
    END DO

!when completely damaged, normal residual stiffness => small elasticity
    IF (STATEV(lenD + 2) .EQ. 1.d0) THEN
      ddsdde(1,:) = 0.D0
      ddsdde(:,1) = 0.D0
      ddsdde(1,1) = 5.d-5/1.d0 * CT%stiff(1,1)
    END IF

    IF (STATEV(lenD + 3) .EQ. 1.d0) THEN
      ddsdde(3,:) = 0.D0
!     ddsdde(:,3) = 0.D0
      ddsdde(3,3) = 5.d-5/1.d0 * CT%stiff(3,3)
    END IF

    IF ((STATEV(lenD + 2) .EQ. 1.d0) .AND. (STATEV(lenD + 3) .EQ. 1.d0)) THEN
      ddsdde = 5.d-5/1.d0 * CT%stiff
    END IF

  ELSE

  ! calculate ddsdde when explicit scheme is used
    ddsdde = CT%Stiff
    stiff1 = 0.d0
    DO i = 1, numint
       DO j = 1, nsd
          ii = (j-1) * numint + i
          DO k1 = 1, ntens
            stiff1(j,i,k1) = fjac2(ii, k1)
          END DO
       END DO
    END DO

    DO i = 1, numint
       DO j = 1, nsd
          DO k1 = 1, ntens
             DO k2 = 1, ntens
               ddsdde(k1,k2) = ddsdde(k1,k2) + CT%RBar(i,j,k1) * stiff1(j,i,k2)
             END DO
          END DO
       END DO
    END DO

  END IF


20 format( 16(E12.5,2x) )
30 format( 100(E12.5,2x) )
  Return

END SUBROUTINE DDSDDE_UPDATE

