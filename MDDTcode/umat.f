INCLUDE 'modules2.f'
INCLUDE 'uexternaldb.f90'
INCLUDE 'ludecomp.f90'
!INCLUDE 'inv_tensor.f'
!INCLUDE 'ct_modification.f'
INCLUDE 'solverom.f'
INCLUDE 'KnewTsolver.f'
!INCLUDE 'explicit.f'
INCLUDE 'urdfil.f'


SUBROUTINE UMAT(STRESS,STATEV,DDSDDE,SSE,SPD,SCD,&
     RPL,DDSDDT,DRPLDE,DRPLDT,&
     STRAN,DSTRAN,TIME,DTIME,TEMP,DTEMP,PREDEF,DPRED,CMNAME,&
     NDI,NSHR,NTENS,NSTATV,PROPS,NPROPS,COORDS,DROT,PNEWDT,&
     CELENT,DFGRD0,DFGRD1,NOEL,NPT,LAYER,KSPT,JSTEP,KINC)

  USE INP_COMP
  USE DATATYPES
  USE COEFTENS
  USE TIMING


  INCLUDE 'aba_param.inc'

  CHARACTER*80 CMNAME

  !IMPLICIT NONE

  ! Standard UMAT variables
  INTEGER, INTENT(IN) :: NTENS,NSTATV,NPROPS,NDI,NSHR,NOEL,NPT,KSPT,KINC,LAYER
  INTEGER, INTENT(IN) :: JSTEP(4) ! This is valid for Abaqus 6.14

  INTEGER :: KSTEP

  CHARACTER*80 CFRP_umat ! Name of the umat file for CFRP material

  REAL(KIND=8) :: STRESS(NTENS),STATEV(NSTATV),&
       DDSDDE(NTENS,NTENS),DDSDDT(NTENS),DRPLDE(NTENS),&
       STRAN(NTENS),DSTRAN(NTENS),TIME(2),PREDEF(1),DPRED(1),&
       PROPS(NPROPS),COORDS(3),DROT(3,3),DFGRD0(3,3),DFGRD1(3,3)

  REAL(KIND=8) :: SSE,SPD,SCD,RPL,DRPLDT,TEMP,DTEMP,PNEWDT,CELENT
  REAL(KIND=8) :: DTIME
  REAL(KIND=8) :: FJ(lenD_ex,lenD_ex),FJ2(lenD_ex,NTENS)

  ! Updated state variables
  REAL(KIND=8) :: stvout(nstatv)

  ! Material parameter structure
  TYPE(INTFCPARM) :: INTPR(num_intfc)

  ! Coefficient tensor structure
  TYPE(CoefTensStr) :: CT

  ! Counters
  INTEGER :: iph,ictr,I,J,K,flag
  INTEGER :: NUMINT2, NSD, NumInt, lenD

  !for checking
  INTEGER :: noel_check

  !for passing variable from uexternaldb
  REAL(KIND=8) :: OMEGA_AP(NumInt_ex), OMEGA_INI(NumInt_ex), DELTA_INI(NUMINT2_ex), cparm, penparm
  INTEGER :: mono_t, QS_t, multemp_t, micro_t, E_INTFC_t(NumInt_ex)

  noel_check = 0

!	Read a1, a2, C
  DO IINT = 1,NUM_INTFC
     INTPR(IINT)%a1Int = INTFCPROP(IINT)%a1Int
     INTPR(IINT)%a2Int = INTFCPROP(IINT)%a2Int
     INTPR(IINT)%ckInt(1:2) = INTFCPROP(IINT)%ckInt(1:2)
     INTPR(IINT)%ckInt(3) = INTFCPROP(IINT)%ckInt(3)
     INTPR(IINT)%eqvStnIniInt = INTFCPROP(IINT)%eqvStnIniInt
	   INTPR(IINT)%gamma = INTFCPROP(IINT)%gamma
  END DO

  ! if (noel.eq.noel_check) then
  !   write(7,*) '===================================='
  !   write(7,*) 'a1Int'
  !   write(7,*) INTPR(1)%a1Int, INTPR(2)%a1Int, INTPR(3)%a1Int
  !   write(7,*) 'a2Int'
  !   write(7,*) INTPR(1)%a2Int, INTPR(2)%a2Int, INTPR(3)%a2Int
  !   write(7,*) 'ckInt1'
  !   write(7,*) INTPR(1)%ckInt(1), INTPR(2)%ckInt(1), INTPR(3)%ckInt(1)
  !   write(7,*) 'ckInt2'
  !   write(7,*) INTPR(1)%ckInt(2), INTPR(2)%ckInt(2), INTPR(3)%ckInt(2)
  !   write(7,*) 'gamma'
  !   write(7,*) INTPR(1)%gamma, INTPR(2)%gamma, INTPR(3)%gamma
  !   write(7,*) '===================================='
  ! end if

  cparm = c_parm
  penparm = pen_parm

  mono_t = MONO
  QS_t = QS
  multemp_t = multemp
  micro_t = micro
  E_INTFC_t = E_INTFC

  NSD = NSD_ex
  NumInt = NUMINT_ex
  lenD = LEND_ex
  NumInt2 = NSD_ex*NumInt_ex


  !if (noel.eq.1) then
  !  write(*,"(A,I10.2,A,f25.17,A,f19.17)") ' Increment No.: ', KINC, '    Time:', time(1),'    Incremental Time:', dtime
  !end if

!	Read Coefficient Tensor

  ALLOCATE (CT%STIFF( NTENS, NTENS ))
  ALLOCATE (CT%RBAR(NUMINT,NSD,NTENS),CT%RBAR_HATHAT(NUMINT,NSD,NTENS))
  ALLOCATE (CT%DE( NUMINT2, NUMINT2),CT%DE_HATHAT(NUMINT2, NUMINT2))
  ALLOCATE (CT%CC(NSD, NUMINT,NTENS))

  CT%Stiff = CF_T%Stiff
  CT%RBAR = CF_T%RBAR
  CT%CC = CF_T%CC
  CT%DE = CF_T%DE

  ! Race condition to define the processor ID
!  IF (.NOT. ini_pro_id) THEN
!     if ((noel.eq.1).and.(npt.eq.1).and.(iph.eq.1)) write(*,*) 'save to hold'
!     ini_pro_id = .TRUE.
!     pro_id = NOEL
!  END IF


    IF ((mono_t == 0).and.(multemp_t == 1)) then
      !obtain delta_0 and w_0 in micro step
      IF (micro_t == 1) THEN
        IF (KINC == KINC_INI) THEN
          DELTA_HOLD(NOEL,NPT,:) = (/ STATEV( 1 : lenD ) /)
          ictr =lenD
          DO i = 1,NUM_INTFC
            if (E_INTFC(i) == 1) then
              OMEGA_HOLD(NOEL,NPT,i) = STATEV(ictr+i)
            endif
          enddo

        END IF

        DELTA_INI = DELTA_HOLD(NOEL,NPT,:)

      !obtain w_ap in macro step
      ELSE
        OMEGA_AP = 0.d0
        DO i = 1,NUM_INTFC
          if (E_INTFC(i) == 1) then
            OMEGA_AP(i) = OMEGA_DEL(noel,npt,i)
          endif
        enddo

      ENDIF

    ENDIF


  IF (CMNAME.EQ.'ELASMAT') THEN

     stran = stran + dstran
     stress = MATMUL(CT%stiff, stran)
     ddsdde = CT%stiff
     STATEV = 0.d0

   else

      CALL ROM(STRAN, DSTRAN, STRESS, FJ, FJ2, STATEV, STVOUT, NTENS, NDI, NSTATV, NUMINT2, NSD, NumInt, lenD,&
                   KINC, DTIME, NOEL, NPT, INTPR, PNEWDT, CT, flag, noel_check, &
                   cparm, penparm, OMEGA_AP, OMEGA_INI, DELTA_INI, mono_t, QS_t, multemp_t, micro_t, E_INTFC_t)

      STATEV = STVOUT ! Update the state variables for the next increment

      ! Record damage(omega) at the end of increment step
      IF ((mono_t == 0).and.(multemp_t == 1)) THEN
        IF (micro_t == 1) THEN
            ictr =lenD
            DO i = 1,NUM_INTFC
              if (E_INTFC(i) == 1) then
                OMEGA_DATA(NOEL,NPT,i) = STATEV(ictr+i)
              endif
            enddo
            !OMEGA_DATA(NOEL,NPT,:) = STATEV(ictr+1 : ictr + numint)
        ENDIF
      ENDIF

      CALL DDSDDE_UPDATE(STRAN,DSTRAN,FJ,FJ2,DTEMP,NTENS,DDSDDE, STATEV, NSTATV, NSTATV_PH, NUMINT2, NSD, NumInt, lenD, NOEL, CT, flag)

  end if

!  if ((noel.eq.2383).or.(noel.eq.2384).or.(noel.eq.2385).or.(noel.eq.2386)) then
!     write(7,*) 'noel', noel
!	 write(7,*) 'matrixdmg', STATEV(11)
!  end if

10 format( 4(E12.5,2x) )
20 format( 100(E25.18,2x) )
25 format( 100(E17.10,2x) )
30 format( 8(E12.5,2x) )
  RETURN

END SUBROUTINE UMAT


!***********************************************************************
!***********************************************************************


SUBROUTINE PRINTTIME(noel,npt,time,Num_Print_Elem)

! Print_time profile the compute time of the after every Num_Print_Elem
! element computation is finished

  implicit none

  ! Variables for printing the current time
  INTEGER, INTENT(IN) :: Num_Print_Elem
  INTEGER,INTENT(IN) :: NOEL,NPT
  REAL*8,INTENT(IN) :: TIME(2)

  CHARACTER(8) :: DATE
  CHARACTER(10) :: CURRENTTIME
  CHARACTER(5) :: ZONE
  INTEGER,DIMENSION(8) :: VALUES

  IF ((NOEL.ne.1).and.(MOD(NOEL,Num_Print_Elem) == 1).and.(NPT == 1)) THEN
     CALL DATE_AND_TIME(DATE,CURRENTTIME,ZONE,VALUES)
     WRITE(*,"(A,I6,A,E16.9,A,I2,A,I2,A,I2,A,I3)")&
          'El: ',NOEL,' time: ',TIME(2),' real: ',  VALUES(5),&
          ':', VALUES(6), ':', VALUES(7), '.', VALUES(8)
  END IF

END SUBROUTINE PRINTTIME



!SUBROUTINE UVARM(uvar,direct,t,time,dtime,cmname,orname, &
!     nuvarm,noel,npt,layer,kspt,kstep,kinc,ndi,nshr,coord, &
!     jmac,jmatyp,matlayo,laccfla)

!  USE KVARM

!  INCLUDE 'aba_param.inc'

!  CHARACTER*80 :: cmname,orname
!  CHARACTER*3 :: flgray(15)
!  DIMENSION :: uvar(nuvarm),direct(3,3),t(3,3),time(2)
!  DIMENSION :: array(15),jarray(15),jmac(*),jmatyp(*),coord(*)


!  ii = 0

!  DO i=1,nintP
!     uvar( i + ii ) = dmgIntP(i)
!  END DO
!  ii = ii + nintP

!  DO i= 1 , nphP
!  	 uvar(i + ii) = dmgPhP(I)
!  END DO

!  ii = ii + nphP


!  DO i=1 , nsdP * nintP
!     uvar(i + ii) = rdispP(i)
!     uvar(i + ii + nsdP * nintP) = tracP(i)
!  END DO



!END SUBROUTINE UVARM

