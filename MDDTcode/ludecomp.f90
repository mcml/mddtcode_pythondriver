MODULE KNR

  INTERFACE

     SUBROUTINE KLUDCMP(a,indx,d)

       REAL(KIND=8), DIMENSION(:,:), INTENT(INOUT) :: a
       INTEGER, DIMENSION(:), INTENT(OUT) :: indx
       REAL(KIND=8), INTENT(OUT) :: d

     END SUBROUTINE KLUDCMP

  END INTERFACE

  INTERFACE

     SUBROUTINE KLUBKSB(a,indx,b)

       REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: a
       INTEGER, DIMENSION(:), INTENT(IN) :: indx
       REAL(KIND=8), DIMENSION(:), INTENT(INOUT) :: b

     END SUBROUTINE KLUBKSB

  END INTERFACE

END MODULE KNR

!-----------------------------------------------------------------------
! Subroutine: KLUDCMP -> LU Decomposition
!   Numerical Recipes Library Routine
!-----------------------------------------------------------------------


SUBROUTINE KLUDCMP(a,indx,d)

    Implicit none

  REAL*8, DIMENSION(:,:), INTENT(INOUT) :: a
  INTEGER, DIMENSION(:), INTENT(OUT) :: indx
  REAL*8, INTENT(OUT) :: d

!Given an N � N input matrix a, this routine replaces it by the LU decomposition of a
!rowwise permutation of itself. On output, a is arranged as in equation (2.3.14); indx is an
!output vector of length N that records the row permutation effected by the partial pivoting;
!d is output as �1 depending on whether the number of row interchanges was even or odd,
!respectively. This routine is used in combination with lubksb to solve linear equations or
!invert a matrix.

  REAL*8, DIMENSION(SIZE(a,1)) :: vv ! vv stores the implicit scaling of each row.
  REAL*8, PARAMETER :: TINY=1.0d-20 ! A small number.
  INTEGER :: j,n,imax

  REAL*8, DIMENSION(SIZE(a,1)) :: dum ! dummy variable for swapping of rows
  INTEGER, DIMENSION(SIZE(a,1)) :: imaxtmp ! dummy variable for maximum locator
  
!  REAL*8, ALLOCATABLE :: y1(:,:), y2(:,:)

  n = size(a,1)

  d=1.0 ! No row interchanges yet.

  vv=MAXVAL( DABS(a),dim=2 ) ! Loop over rows to get the implicit scaling information

  IF ( ANY(vv == 0.0)) THEN
     WRITE(7,*) 'singular matrix in ludcmp' ! There is a row of zeros.
     CALL XIT
  END IF

  vv=1.d0/vv ! Save the scaling.

  DO j=1,n
   
     imaxtmp = MAXLOC( vv(j:n) * DABS( a(j:n,j) ) )

     imax=(j-1) + imaxtmp(1) ! Find the pivot row.

     IF (j /= imax) THEN      ! Do we need to interchange rows?

        dum = a(imax,:)
        a(imax,:) = a(j,:)
        a(j,:) = dum
        d=-d                         ! ...and change the parity of d.
        vv(imax)=vv(j)            !Also interchange the scale factor.

     END IF

     indx(j)=imax
     IF (a(j,j) == 0.0) a(j,j)=TINY

!If the pivot element is zero the matrix is singular (at least to the precision of the algorithm).
!For some applications on singular matrices, it is desirable to substitute TINY
!for zero.

     a(j+1:n,j)=a(j+1:n,j)/a(j,j) ! Divide by the pivot element.

!     ALLOCATE( y1(n-j,n-j), y2(n-j,n-j) )
!     y1 = 0.d0
!     y2 = 0.d0
!     DO k1 = 1, n-j
!        DO k2 = 1, n-j
!           y1(k1,k2) = a( k1 + j , j  )
!           y2(k2,k1) = a( j, k1 + j )
!        END DO
!     END DO

!     a(j+1:n,j+1:n)=a(j+1:n,j+1:n) - y1 * y2

!     DEALLOCATE(y1, y2)

    a(j+1:n,j+1:n)=a(j+1:n,j+1:n) - SPREAD( a(j+1:n,j), dim=2, ncopies=n-j ) * &
          SPREAD( a(j,j+1:n), dim=1, ncopies=n-j ) !Reduce remaining submatrix.

  END DO

END SUBROUTINE KLUDCMP


!-----------------------------------------------------------------------
! Subroutine: KLUBKSB -> LU Back Substitution
!   Numerical Recipes Library Routine
!-----------------------------------------------------------------------

SUBROUTINE KLUBKSB(a,indx,b)

    implicit none

!  INCLUDE 'ABA_PARAM.INC'
  
  REAL*8, DIMENSION(:,:), INTENT(IN) :: a
  INTEGER, DIMENSION(:), INTENT(IN) :: indx
  REAL*8, DIMENSION(:), INTENT(INOUT) :: b

!Solves the set of N linear equations A � X = B. Here the N � N matrix a is input, not
!as the original matrix A, but rather as its LU decomposition, determined by the routine
!ludcmp. indx is input as the permutation vector of length N returned by ludcmp. b is
!input as the right-hand-side vector B, also of length N, and returns with the solution vector
!X. a and indx are not modified by this routine and can be left in place for successive calls
!with different right-hand sides b. This routine takes into account the possibility that b will
!begin with many zero elements, so it is efficient for use in matrix inversion.

  INTEGER :: i,n,ii,ll
  REAL*8 :: summ

  n = SIZE(a,1)
  ii=0 ! When ii is set to a positive value, it will become the index
       ! of the first nonvanishing element of b. We now do
       ! the forward substitution, equation (2.3.6). The only new
       ! wrinkle is to unscramble the permutation as we go.
  DO i=1,n
     ll=indx(i)
     summ=b(ll)
     b(ll)=b(i)

     IF (ii /= 0) THEN
        summ=summ-DOT_PRODUCT(a(i,ii:i-1),b(ii:i-1))
     ELSE IF (summ /= 0.0) THEN
        ii=i        ! A nonzero element was encountered, so from now on we will
     END IF         ! do the dot product above.

     b(i)=summ

  END DO
    
  DO i=n,1,-1       ! Now we do the backsubstitution, equation (2.3.7).
     b(i) = (b(i)-DOT_PRODUCT(a(i,i+1:n),b(i+1:n)))/a(i,i)
  END DO

END SUBROUTINE KLUBKSB
