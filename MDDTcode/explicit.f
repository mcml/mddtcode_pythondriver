SUBROUTINE EXPLICIT(D, dold, fjac2, dmgInt, dmgIntOld, stran, dstran, INTPR, CT, NTENS)

  USE INP_COMP
  USE COEFTENS
  USE DATATYPES
  USE KPARM
  USE KNR
  
  INCLUDE 'aba_param.inc'

    REAL(KIND=8) :: D(lenD), fjac_FD(lenD,lenD), FJAC2(lenD,NTENS), drhs(lenD), F(lenD,NTENS)
    REAL(KIND=8) :: dmgInt(numint), dOld(lenD)
	REAL(KIND=8) :: STRAN(NTENS), DSTRAN(NTENS), STRAN_FD(NTENS)
! -------- For interface damage ------------------------------------
    REAL(KIND=8) :: eqvStnInt(numint), tngNorm(numint)
    REAL(KIND=8) :: dmgIntNew, dmgIntOld(numint), ldmgInt(numint)
    REAL(KIND=8) :: delDmgInt(numint) 	
    REAL(KIND=8) :: ck( lenD, lenD ),ck_FD( lenD, lenD )
!----------- Derivative terms -----------------------------------------
    REAL(KIND=8) :: ckDerD(lenD,lenD), eqvStnIntD(nsd), dmgIntDer(nsd)
!----------------------------------------------------------------------
    REAL(KIND=8) :: d_FD(nsd), epsilon_FD, epsilon_FD2, tngNorm_FD, eqvStnInt_FD, dmgInt_FD
    INTEGER :: I, J, K, ii, I1, J1, K1, K2, ictr, iDir
    INTEGER :: NTENS, NDI, noel, npt, rdInd, istran
    TYPE(INTFCPARM) :: INTPR(NUM_INTFC)
    TYPE(CoefTensStr), INTENT(IN) :: CT
	REAL(KIND=8), PARAMETER :: dmgIntMax = 1.d0
!----------------------------------------------------------------------
	INTEGER, DIMENSION( lenD ) :: indx
    REAL*8, DIMENSION( lenD ) :: p,x
	
	epsilon_FD = 1d-10
	epsilon_FD2 = 1d-10
	
! Finite difference calculation of the interface damage derivative contribution	dw/d(delta)
  ck_FD = 0.d0
  ckDerD = 0.d0
  DO i = 1, numint

     DO iDir = 1,nsd
        ! set the appropriate delta vector 
        DO j = 1, nsd
           d_FD(j) = d(rdInd + (j-1)*numint + i )
        END DO
        ! perturb the appropriate term in the delta vector
        d_FD(iDir) = d_FD(iDir) + epsilon_FD 

        ! Compute damage equivalent strain
        tngNorm_FD = 0.d0
        DO j = 2, nsd
           tngNorm_FD = tngNorm_FD + d_FD( j )**2.d0
        END DO
        tngNorm_FD = DSQRT( tngNorm_FD )
     
        eqvStnInt_FD = IntPr(i)%ckInt(1) * (DABS( d_FD(1) ) + d_FD(1)) / 2.d0 &
             + IntPr(i)%ckInt(2) * tngNorm_FD
        eqvStnInt_FD = DDIM( eqvStnInt_FD, IntPr(i)%eqvStnIniInt )

        ! Compute the damage value
        dmgInt_FD = DATAN( IntPr(i)%a1Int * eqvStnInt_FD ) / DATAN( IntPr(i)%a1Int * IntPr(i)%a2Int )
    
        IF (dmgInt_FD < dmgIntOld(i)) THEN
           dmgInt_FD = dmgIntOld(i)
        END IF
    
        IF (dmgIntOld(i).eq.0.d0) THEN
           dmgInt_FD = dmgIntOld(i)
        END IF

        IF ( dmgInt_FD > dmgIntMax ) THEN
           dmgInt_FD = dmgIntMax
        END IF
        
        ! Compute the finite difference derivative
        dmgIntDer(iDir) = (dmgInt_FD - dmgIntOld(i)) / epsilon_FD
		
     END DO
	
     DO j = 1, nsd
        j1 = rdInd + (j-1) * numint + i
        ckDerD(j1,j1) = - dmgIntDer(j) * IntPr(i)%ckInt(j) * d( j1 )
			
     END DO
	 
  END DO
  
! get (1 - w) * k + D - dw/d(delta) * k *delta
  DO i = 1, numint
     DO ii = 1, nsd
        i1 = rdInd + i + (ii-1)*numint
        ck_FD(i1, i1) = ( 1.d0 - dmgIntOld(i) ) * IntPr(i)%ckInt(ii)
     END DO
  END DO
  
  fjac_FD = ck_FD + CT%de + ckDerD

	
 ! get d(delta)/d(stran)	using finite difference method
  DO istran = 1, NTENS
  
  	STRAN_FD = STRAN
	STRAN_FD(istran) = stran(istran) + epsilon_FD2

	DO ii = 1, nsd
		DO i = 1, numint
			drhs(i+(ii-1)*numint) =  - DOT_PRODUCT( CT%cc(ii,i,:), STRAN_FD - STRAN)
		END DO
	END DO
	
	p(:) = drhs(:)                  		!Right-hand side for linear equations.
    CALL KLUDCMP(fjac_FD,indx,dTemp)       !Solve linear equations by LU decomposition.
    CALL KLUBKSB(fjac_FD,indx,p)
	
	
	
	DO i = 1, nsd
		DO j = 1, numint
	 		F(j + (i - 1)*numint, istran) = p(j + (i - 1)*numint)/epsilon_FD2
		END DO
	END DO
	
  END DO
	
! update delta
	DO i = 1, nsd
		DO j = 1, numint
  			d(j + (i - 1)*numint) = dOld(j + (i - 1)*numint) + DOT_PRODUCT(F(j + (i - 1)*numint,:), dstran)
		END DO
	END DO

! update damage
  	DO i = 1, numint

        eqvStnInt(i) = IntPr(i)%ckInt(1) * (DABS( d( rdInd + i ) ) + d( rdInd + i )) / 2
        tngNorm(i) = 0.d0
        DO j = 2, nsd
           tngNorm(i) = tngNorm(i) + d( rdInd + (j-1)*numint + i )**2.d0
        END DO
        tngNorm(i) = DSQRT( tngNorm(i) )
        eqvStnInt(i) = eqvStnInt(i) + IntPr(i)%ckInt(2) * tngNorm(i)
		
        eqvStnInt(i) = DDIM( eqvStnInt(i), IntPr(i)%eqvStnIniInt )
        dmgIntNew = DATAN( IntPr(i)%a1Int * eqvStnInt(i) ) / DATAN( IntPr(i)%a1Int * IntPr(i)%a2Int )
     
        IF (dmgIntNew > dmgIntOld(i)) THEN
           dmgInt(i) = dmgIntNew
           ldmgInt(i) = 1  ! damage process indicator
        ELSE
           dmgInt(i) = dmgIntOld(i)
           ldmgInt(i) = 0

        END IF
     
        IF ( dmgInt(i) >dmgIntMax ) THEN
           dmgInt(i) = dmgIntMax
           ldmgInt(i) = 2
        END IF
		
	!dmgInt(1) = 0.d0
    !ldmgInt(1) = 0
	!dmgInt(3) = 0.d0
    !ldmgInt(3) = 0
		
	END DO
	
!update jacobian matrix
	fjac2 = F

END SUBROUTINE