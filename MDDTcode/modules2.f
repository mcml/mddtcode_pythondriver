MODULE DATATYPES

!---------------------------------------
! Interface damage parameter datatypes
!---------------------------------------
  Type intfcparm

    REAL(KIND=8):: a1Int, a2Int, ckInt(3), eqvStnIniInt, gamma

  End type intfcparm


  Type CoefTensStr
     REAL(KIND=8), ALLOCATABLE :: LBAR(:,:,:), LHAT(:,:,:)
     REAL(KIND=8), ALLOCATABLE :: PBAR(:,:,:,:), RHAT(:,:,:,:)
     REAL(KIND=8), ALLOCATABLE :: FHAT(:,:,:,:,:), CHAT(:,:,:,:)
     REAL(KIND=8), ALLOCATABLE :: COEFA(:,:,:)
     REAL(KIND=8), ALLOCATABLE :: COEFR(:,:,:,:), KMAT(:,:,:)
     REAL(KIND=8), ALLOCATABLE :: KINI(:,:), PRERHS(:,:,:), PRERHSINI(:,:)
     REAL(KIND=8), ALLOCATABLE :: STIFF(:,:), MBAR(:,:,:)
!    REAL(KIND=8), ALLOCATABLE :: COEFPB(:,:,:), COEFPH(:,:,:,:)
     REAL(KIND=8), ALLOCATABLE :: STIFFA(:,:,:), STIFFM(:,:,:), COEFP(:,:,:,:)
     REAL(KIND=8), ALLOCATABLE :: RBAR(:,:,:),RBAR_HATHAT(:,:,:)
     REAL(KIND=8), ALLOCATABLE :: RR(:,:,:,:), CC(:,:,:)
     REAL(KIND=8), ALLOCATABLE :: FF(:,:,:,:), DE(:,:),DE_HATHAT(:,:)

  End Type CoefTensStr

END MODULE DATATYPES


!module for communication between uexternaldb and umat.f
MODULE INP_COMP

  USE DATATYPES

  CHARACTER(200) :: PATH = 'Mufasa Lives!'

  INTEGER :: NUM_INTFC, NUM_PARTS, NUM_ELES, NUM_INT_PTS_ELE
  INTEGER :: KINC_INI, QS,MONO,multemp, MICRO
  INTEGER, ALLOCATABLE :: E_INTFC(:)
  REAL(KIND = 8) :: c_parm,pen_parm
  REAL(KIND = 8), ALLOCATABLE :: lmicro(:), vfmicro(:), helem(:)
  REAL(KIND = 8), ALLOCATABLE :: OMEGA_DEL(:,:,:), OMEGA_DATA(:,:,:), OMEGA_HOLD(:,:,:), DELTA_HOLD(:,:,:)

! INTFCPARM indicates interface parameters for damage model
  REAL(KIND=8), ALLOCATABLE :: INTFCPARM0(:,:)
  TYPE(INTFCPARM), ALLOCATABLE :: INTFCPROP(:)

  INTEGER :: N_PROPTS
  REAL(KIND=8), ALLOCATABLE :: PROPTS(:),kf(:,:),softslope(:,:)

  CHARACTER(LEN=255) :: jobname, outdir
  INTEGER :: lenjobname, lenoutdir

! Variables for tracking timing and the number of subroutine calls, convergence checking

  LOGICAL :: ini_pro_id
  INTEGER :: pro_id

END MODULE INP_COMP


!module for communication within uexternaldb, between uexternaldb and umat.f
MODULE COEFTENS

  USE DATATYPES, ONLY : CoefTensStr

  INTEGER :: MTDIM, NUMINT_ex, NPH_ex, NSD_ex, LEND_ex, NUMINT2_ex
  INTEGER, ALLOCATABLE :: MT(:)
  TYPE(CoefTensStr) :: CF_T

END MODULE COEFTENS

!module for communication between solverom and urdfil
MODULE CALL_TERMINATION

  LOGICAL :: termination

END MODULE CALL_TERMINATION




MODULE TIMING

  REAL(KIND=8) :: longest_step,printed_step
  INTEGER :: most_psis,printed_psis
  INTEGER :: most_updates,printed_updates
  LOGICAL :: printing

END MODULE TIMING



MODULE allstraindata

  REAL(KIND=8) :: allstrain(24),allstress(24)
  INTEGER(KIND=8) :: allstrain_n

END MODULE allstraindata


!***********************************************************************
! Module: KPARM -> Data Module stores defined parameters
!***********************************************************************

MODULE KPARM

  IMPLICIT REAL*8(a-h,o-z)

  REAL(KIND=8), PARAMETER :: maxit = 1000, errNewton = 1.d-10
  !REAL(KIND=8), PARAMETER :: penParm=1.d-15
  REAL(KIND=8) :: penParm
  REAL(KIND=8), PARAMETER :: penParmMin = 1.d-05
  REAL(KIND=8), PARAMETER :: pi = 4.d0 * DATAN(1.d0)
  INTEGER, PARAMETER :: nspurit = 1
!  REAL(KIND=8), PARAMETER :: zero = 0.d0, one = 1.d0, two = 2.d0, half = 0.5d0
!  REAL(KIND=8), PARAMETER :: a1Int(1) = 6.6667d-04    ! damage law parameter
!  REAL(KIND=8), PARAMETER :: a2Int(1) = 1.0d04    ! damage law parameter
!  REAL(KIND=8), PARAMETER :: eqvStnIniInt(1) = 0.d0  ! damage law parameter

!  REAL(KIND=8), PARAMETER ::   a1Ph(1) = 3.2d01 ,a1Ph(2) = 3.2d01    ! damage law parameter
!  REAL(KIND=8), PARAMETER ::   a2Ph(1) = 1.63d1 ,a2Ph(2) = 1.63d1    ! damage law parameter
!  REAL(KIND=8), PARAMETER ::   eqvStnIniPh(1) = 0.d0 ,eqvStnIniPh(2) = 0.d0 ! damage law parameter
!  REAL(KIND=8), PARAMETER ::   c1Ph(1) = 1.d05 , c1Ph(2) = 1.d05    ! damage law parameter
!  REAL(KIND=8), PARAMETER ::   c2Ph(1) = 0.d0 ,  c1Ph(2) = 0.d0   ! damage law parameter

END MODULE KPARM

!--------------------------------------------------------------
! Module file that contains the function MODMAP
! which is used to map the condensed notation
! to full tensor notation
! dimensionality (nsd) must be supplied to the routine
!---------------------------------------------------------------

MODULE ELASTICMODULUSMAP

CONTAINS

FUNCTION MODMAP(i,j,nsd)

	INTEGER :: i, j, nsd
	INTEGER :: MODMAP

	IF (i == j) THEN
		MODMAP = i
		RETURN
	END IF

	SELECT CASE (nsd)

	CASE (2) ! 2-d plane strain

		MODMAP = 4 ! shear mode


	CASE (3) ! 3-d

		MODMAP = i + j + 1 ! shear modes

	END SELECT

END FUNCTION MODMAP

END MODULE ELASTICMODULUSMAP


MODULE KVARM

  IMPLICIT REAL*8(a-h,o-z)

  DIMENSION :: rdispP(50), tracP(50), dmgIntP(100)
  DIMENSION :: dmgPhP(100), plStnDmgP(200)
  INTEGER :: nintP, nphP, ntensP, nsdP

END MODULE KVARM
