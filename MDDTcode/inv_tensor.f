SUBROUTINE INVERSION(X,X_INV,SIZE)

  USE INP_COMP
  USE COEFTENS
  USE DATATYPES
  USE KPARM
  USE KNR

  INCLUDE 'aba_param.inc' 
  
  INTEGER :: SIZE,i,ii
  INTEGER:: indx(SIZE)
  REAL(KIND=8), INTENT(IN):: X(SIZE,SIZE)
  REAL(KIND=8):: X_TEMP(SIZE,SIZE),IDT(SIZE)
  REAL(KIND=8), INTENT(OUT):: X_INV(SIZE,SIZE)
  
!  ALLOCATE (X(SIZE,SIZE), X_TEMP(SIZE,SIZE), X_INV(SIZE,SIZE), indx(SIZE),IDT(SIZE))
  
  DO i = 1, SIZE
	
	X_TEMP = X
	
	DO ii = 1,SIZE
		IDT(ii) = 0.d0	
	END DO
	
	IDT(i) = 1.d0
	
	CALL KLUDCMP(X_TEMP,indx,d)       !Solve linear equations by LU decomposition.
    CALL KLUBKSB(X_TEMP,indx,IDT)
	
	X_INV(:,i) = IDT(:)
	
  END DO 

20 format( 16(E12.5,2x) )
30 format( 100(E12.5,2x) )
  Return
END SUBROUTINE INVERSION
  