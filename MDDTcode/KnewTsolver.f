!-----------------------------------------------------------------------
! Subroutine: KNEWT -> Globally convergent Newton Method
!   Numerical Recipes Library Routine
!-----------------------------------------------------------------------

SUBROUTINE KNEWT(x, dold, fjac1, dmgInt, dmgIntOld, dmgIntOld_pre, phi, phi_old, phi_old_pre, RHS,&
                 INTPR, CT, NTENS, NDI, NSTATV, NUMINT2, NSD, NumInt, lenD, DTIME, NOEL, NPT, noel_check, &
                 cparm_t, penparm, OMEGA_AP, mono_t, multemp_t, micro_t, E_INTFC_t, check)

  !USE INP_COMP
  !USE COEFTENS
  USE DATATYPES
  USE CALL_TERMINATION
  !USE KPARM
  USE KNR

  INCLUDE 'aba_param.inc'
  INTEGER :: NTENS, NDI, NSTATV, NumInt2, noel, npt, noel_check, NSD, NumInt, lenD, E_INTFC_t(NumInt)
  REAL*8, INTENT(INOUT) :: x(lenD)
  REAL(KIND=8), INTENT(IN) :: DTIME
  LOGICAL, INTENT(OUT) :: check
  REAL(KIND=8) :: FJAC1(lenD,lenD), dmgIntOld(NUMINT), dmgInt(NumInt), dmgIntOld_pre(NUMINT)
  REAL(KIND=8) :: dOld(lenD), RHS(lenD), CPARM
  REAL(KIND=8) :: phi(numint), phi_old(numint), phi_old_pre(numint)
  TYPE(INTFCPARM) :: INTPR(numint)
  TYPE(CoefTensStr), INTENT(IN) :: CT
  REAL(KIND=8) :: OMEGA_AP(NumInt), cparm_t, penparm
  REAL(KIND=8), ALLOCATABLE :: fvec_check(:)
  integer :: mono_t, multemp_t, micro_t

!Given an initial guess x for a root in N dimensions, find the root by a globally
!convergent Newton's method. The length N vector of functions to be zeroed, called
!fvec in the routine below, is returned by a user-supplied routine that must be
!called FUNCV and have the declaration FUNCTION FUNCV(x). The output quantity check
!is false on a normal return and true if the routine has converged to a local minimum
!of the function fmin defined below. In this case try restarting from a different
!initial guess. Parameters: MAXITS is the maximum number of iterations; TOLF sets the
!convergence criterion on function values; TOLMIN sets the criterion for deciding
!whether spurious convergence to a minimum of fmin has occurred; TOLX is the
!convergence criterion on deltax STPMX is the scaled maximum step length allowed in
!line searches.

  INTEGER, DIMENSION( SIZE(x) ) :: indx
  REAL*8, DIMENSION( SIZE(x) ) :: g,p,xold,X_ORI,FVEC_ORI
  REAL*8, DIMENSION( SIZE(x) ), TARGET :: fvec
  REAL*8, DIMENSION( SIZE(x), SIZE(x) ), TARGET :: fjac
  REAL*8, DIMENSION( SIZE(x), SIZE(x) ) :: fjacReg, FJAC_ORI ! For regularized newton method
  REAL*8, DIMENSION( SIZE(x) ) :: eigr,eigi
  PARAMETER ( maxits=5000 )
  PARAMETER ( tolf=1.0d-2,tolmin=1.0d-8,tolx=epsilon(x),stpmx=100 )

  fvec = 0.d0
  fjac = 0.d0
  check = .true.

  X_ORI = X

  CPARM = 0.D0

  IF (noel.eq.noel_check) THEN
    WRITE(7,*) '=========================='
    WRITE(7,*) 'its',0
    WRITE(7,*) 'CPARM',CPARM
    WRITE(7,*) 'func_pre',fvec
    WRITE(7,*) 'delta_pre',x
    WRITE(7,*) '--------------------------'
  END IF

  CALL KFUNJAC( x, fvec, fjac ,fjac1, dmgInt, dmgIntOld, dmgIntOld_pre,&
                phi, phi_old, phi_old_pre, dOld, RHS, INTPR, CT, NTENS, NDI, NSTATV, &
                NUMINT2, NSD, NumInt, lenD, DTIME, NOEL, NPT, noel_check, CPARM, penparm,&
                OMEGA_AP, mono_t, multemp_t, micro_t, E_INTFC_t)

  IF (noel.eq.noel_check) THEN
    WRITE(7,*) 'func_post',fvec
    WRITE(7,*) 'delta_post',x
    WRITE(7,*) 'dmgInt',dmgInt
    WRITE(7,*) 'dmgIntOld',dmgIntOld
    WRITE(7,*) 'convergence metric', MAXVAL(DABS( FVEC(:))), TOLF
    WRITE(7,*) '=========================='
  END IF

  IF ( MAXVAL( DABS(fvec(:) ) ) < tolf) THEN ! Test for initial guess being a root.
     check=.false.
     RETURN
  END IF

  DO its=1,maxits ! Start of iteration loop.
    xold(:)=x(:)                   !Store x,
    fold=f                         !and f.
    p(:)= - fvec(:)                  !Right-hand side for linear equations.
    CALL KLUDCMP(fjac,indx,d)        !Solve linear equations by LU decomposition.
    CALL KLUBKSB(fjac,indx,p)

    IF (noel.eq.noel_check) THEN
      WRITE(7,*) '=========================='
      WRITE(7,*) 'its',its
      WRITE(7,*) 'CPARM',CPARM
      WRITE(7,*) 'func_pre',fvec
      WRITE(7,*) 'delta_pre',x
      WRITE(7,*) 'dmgInt',dmgInt
      WRITE(7,*) '--------------------------'
    ENDIF


    xold(:) = x(:)
    x(:) = xold(:) + p(:)

    CALL KFUNJAC( x, fvec, fjac ,fjac1, dmgInt, dmgIntOld, dmgIntOld_pre,&
                  phi, phi_old, phi_old_pre, dOld, RHS, INTPR, CT, NTENS, NDI, NSTATV, &
  		  NUMINT2, NSD, NumInt, lenD, DTIME, NOEL, NPT, noel_check, CPARM, penparm,&
                  OMEGA_AP, mono_t, multemp_t, micro_t, E_INTFC_t)

    IF (noel.eq.noel_check) THEN
      WRITE(7,*) 'func_post',fvec
      WRITE(7,*) 'fjac_post',fjac
      WRITE(7,*) 'delta_post',x
      WRITE(7,*) 'dmgInt',dmgInt
      WRITE(7,*) 'dmgIntOld',dmgIntOld
      WRITE(7,*) 'convergence metric', MAXVAL(DABS( FVEC(:))), TOLF
      WRITE(7,*) '=========================='
    END IF

    IF ( MAXVAL( DABS(fvec(:) ) ) < tolf) THEN ! Test for initial guess being a root.
      check=.false.
      RETURN
    END IF

  END DO

 !============================if iterations are not converged with cparm=0, cparm>0 is runned
  CPARM = cparm_t
  fvec = 0.d0
  fjac = 0.d0
  X = X_ORI

  IF (noel.eq.noel_check) THEN
      WRITE(7,*) '=========================='
      WRITE(7,*) 'its',0
      WRITE(7,*) 'CPARM',CPARM
      WRITE(7,*) 'func_pre',fvec
      WRITE(7,*) 'delta_pre',x
      WRITE(7,*) '--------------------------'
  END IF

  CALL KFUNJAC( x, fvec, fjac ,fjac1, dmgInt, dmgIntOld, dmgIntOld_pre,&
                phi, phi_old, phi_old_pre, dOld, RHS, INTPR, CT, NTENS, NDI, NSTATV, &
                NUMINT2, NSD, NumInt, lenD, DTIME, NOEL, NPT, noel_check, CPARM, penparm,&
                OMEGA_AP, mono_t, multemp_t, micro_t, E_INTFC_t)

  IF (noel.eq.noel_check) THEN
      WRITE(7,*) '--------------------------'
      WRITE(7,*) 'func_post',fvec
      WRITE(7,*) 'fjac_post',fjac
      WRITE(7,*) 'delta_post',x
      WRITE(7,*) 'dmgInt',dmgInt
      WRITE(7,*) 'dmgIntOld',dmgIntOld
      WRITE(7,*) 'convergence metric', MAXVAL(DABS( FVEC(:))), TOLF
      WRITE(7,*) '=========================='
  END IF

  IF ( MAXVAL( DABS(fvec(:) ) ) < tolf) THEN ! Test for initial guess being a root.
     check=.false.
     RETURN
  END IF

  DO its=1,maxits ! Start of iteration loop.

    p(:)= - fvec(:)                 !Right-hand side for linear equations.
    CALL KLUDCMP(fjac,indx,d)       !Solve linear equations by LU decomposition.
    CALL KLUBKSB(fjac,indx,p)

!=================================================

    IF (noel.eq.noel_check) THEN
      WRITE(7,*) '=========================='
      WRITE(7,*) 'its',its
      WRITE(7,*) 'CPARM',CPARM
      WRITE(7,*) 'func_pre',fvec
      WRITE(7,*) 'delta_pre',x
      WRITE(7,*) 'dmgInt',dmgInt
      WRITE(7,*) '--------------------------'
    END IF


    xold(:) = x(:)
    x(:) = xold(:) + p(:)

    CALL KFUNJAC( x, fvec, fjac ,fjac1, dmgInt, dmgIntOld, dmgIntOld_pre,&
                phi, phi_old, phi_old_pre, dOld, RHS, INTPR, CT, NTENS, NDI, NSTATV, &
                NUMINT2, NSD, NumInt, lenD, DTIME, NOEL, NPT, noel_check, CPARM, penparm,&
                OMEGA_AP, mono_t, multemp_t, micro_t, E_INTFC_t)

!=================================================

    IF (noel.eq.noel_check) THEN
      WRITE(7,*) '--------------------------'
      WRITE(7,*) 'func_post',fvec
      WRITE(7,*) 'delta_post',x
      WRITE(7,*) 'dmgInt',dmgInt
      WRITE(7,*) 'dmgIntOld',dmgIntOld
      WRITE(7,*) 'arc range',1/2**0.5*cParm,-cParm
      WRITE(7,*) 'convergence metric', MAXVAL(DABS( FVEC(:))), TOLF
      WRITE(7,*) '=========================='
    END IF

    IF ( MAXVAL( DABS(fvec(:) ) ) < tolf) THEN ! Test for initial guess being a root.
      check=.false.
      RETURN
    END IF

  END DO


  WRITE(7,*) 'MAXITS exceeded in newt'
  WRITE(7,*),'maxits',its
  WRITE(7,*) 'noel',noel
  !WRITE(*,*) 'MAXITS exceeded in newt'
  !WRITE(*,*),'maxits',its
  !WRITE(*,*) 'noel',noel
  !WRITE(*,*) 'MAXITS exceeded in newt'
  !WRITE(*,*),'maxits',its
  !WRITE(*,*) 'noel',noel
  !check = .true.
  !CALL xit

20 format( 100(E25.18,2x) )


END SUBROUTINE KNEWT


!-----------------------------------------------------------------------
! Subroutine: KFUNJAC -> function and jacobian evaluations
!             Input: rdisp (Displacement Jumps)
!                    dmg   (Damage variables)
!            Output: fun   (function)
!                    fjac   (jacobian of the function)
!                    dmg   (Updated damage variables)
!-----------------------------------------------------------------------

SUBROUTINE KFUNJAC( D, fun, fjac, fjac1, dmgInt, dmgIntOld, dmgIntOld_pre,&
                    phi, phi_old, phi_old_pre, dOld, RHS, INTPR, CT, NTENS, NDI, NSTATV,&
                    NUMINT2, NSD, NumInt, lenD, DTIME, NOEL, NPT, noel_check, CPARM, penparm,&
                    OMEGA_AP, mono_t, multemp_t, micro_t, E_INTFC_t)

  !USE COEFTENS
  !USE INP_COMP
  USE DATATYPES
  !USE AllStrainData
  !USE KPARM

  implicit none

  REAL(KIND=8) :: D(lenD), dOld(lenD) , FJAC(lenD,lenD), FUN(lenD), FJAC1(lenD,lenD), RHS(lenD)
  REAL(KIND=8) :: dmgInt(numint), dmgIntOld(numint),dmgInt_temp(numint),dmgIntOld_pre(NUMINT)
  REAL(KIND=8) :: eqvStnInt(numint), tngNorm(numint), eqvStnIntold(numint),tngNormold(numint)
  REAL(KIND=8) :: phi(numint), phi_old(numint),phi_dot(numint),phi_old_pre(numint), phi_dot_central(numint)
  REAL(KIND=8) :: phi_omega_gamma(numint),phi_FD_temp(numint),d_omega(numint),d_omega_temp(numint)
  REAL(KIND=8) :: cParm,rParm,rParm_FD,cParm_FD, DTIME, a_omega,b_omega,x_omega,gamma_omega
!-------------------------------------------------------------------
  REAL(KIND=8) :: ck( lenD, lenD ), ckNoDmg( lenD, lenD ), rhsDmg( lenD )
  REAL(KIND=8) :: func (lenD), funcDer(lenD,lenD),rhsDmgDer(lenD, lenD)
!----------- Derivative terms -----------------------------------------
  REAL(KIND=8) :: ckDerD(lenD,lenD), eqvStnIntD(nsd), dmgIntDer(nsd),tmpDer
  REAL(KIND=8) :: d_FD(nsd), tngNorm_FD, eqvStnInt_FD, dmgInt_FD, phi_FD, phi_dot_FD,phi_dot_m_FD
!----------------------------------------------------
  INTEGER :: I, J, K, ii, I1, J1, K1, K2, ictr, iDir, NSD, NumInt, lenD
  INTEGER :: NTENS, NDI, NSTATV, NUMINT2, NOEL, NPT, rdInd, noel_check
  INTEGER :: E_INTFC_t(NumInt),flag_central(numint), ldmgInt(numint), ldmgInt_c(numint), dmgInt_ind(numint)
!----------------------------------------------------
  TYPE(INTFCPARM) :: INTPR(numint)
  TYPE(CoefTensStr), INTENT(IN) :: CT
  REAL(KIND=8) :: OMEGA_AP(NumInt), penparm
  INTEGER :: mono_t, multemp_t, micro_t, mono_ind
  REAL(KIND=8), PARAMETER :: dmgIntMax = 1.d0, epsilon_FD = 1.d-12, pi = 3.141592653

  rParm = tan(3.d0*pi/8.d0)*cParm
  cParm_FD = cParm
  rParm_FD = tan(3.d0*pi/8.d0)*cParm_FD

  IF (NSD==2) THEN
    NTENS = 4
  ELSE
    NTENS = 6
  END IF

!*****************************************************************
! Step(1): Compute Damage evolution
!*****************************************************************
  DO i = 1, numint
  ! Get equivalent strain
    eqvStnInt(i) = IntPr(i)%ckInt(1) * (DABS( d(i) ) + d(i)) / 2
    tngNorm(i) = 0.d0

    DO j = 2, nsd
      tngNorm(i) = tngNorm(i) + d( (j-1)*numint + i )**2.d0
    END DO

    tngNorm(i) = DSQRT( tngNorm(i) )
    eqvStnInt(i) = eqvStnInt(i) + IntPr(i)%ckInt(2) * tngNorm(i)
    eqvStnInt(i) = DDIM( eqvStnInt(i), IntPr(i)%eqvStnIniInt )
  ! Get damage function phi
    phi(i) = DATAN( IntPr(i)%a1Int * eqvStnInt(i) ) / DATAN( IntPr(i)%a1Int * IntPr(i)%a2Int )

    !----------Damage evolution for fatigue case----------
    IF (mono_t == 0) THEN

      phi_dot(i) = (phi(i) - phi_old(I))
      b_omega = (phi(I) + phi_old(I))**INTPR(I)%gamma * phi_dot(i)
      a_omega = dmgIntOld(i)
      x_omega = dmgIntOld(i) + (phi_old(I)/dmgIntOld(i))**INTPR(I)%gamma * phi_dot(i)
      gamma_omega = INTPR(I)%gamma

      !----------Multiple temporal scheme----------
      IF (multemp_t == 1) THEN
        ! In the microstep, or when delD.dat cannot be read, OMEGA_DEL(noel,npt,I) = 0.d0
        IF (dmgIntOld(i) == 0) then
          dmgInt_temp(i) = phi(I)
        ELSE
          CALL KNEWT_OMEGA(b_omega,a_omega,x_omega,gamma_omega)
          dmgInt_temp(i) = x_omega

          !In macro step, add almost periodicity term
          IF (micro_t == 0) THEN
            dmgInt_temp(i) = dmgInt_temp(i) + Dtime * OMEGA_AP(i)
          ENDIF
        ENDIF

      ENDIF

      !----------Cycle-by-cycle loading----------
      IF (multemp_t == 0) THEN
        IF (dmgIntOld(i) == 0) then
          dmgInt_temp(i) = phi(I)
        ELSE
          CALL KNEWT_OMEGA(b_omega,a_omega,x_omega,gamma_omega)
          dmgInt_temp(i) = x_omega
        ENDIF
      ENDIF

    ENDIF

    !----------Damage evolution for static case----------
    IF (mono_t == 1) THEN
      dmgInt_temp(i) = phi(i)
    ENDIF

    !----------Apply Maucaulay bracket----------
    d_omega_temp(I) = dmgInt_temp(i) - dmgIntOld(i)
    IF (d_omega_temp(I) < -cParm) THEN
      d_omega(I) = 0.d0
      ldmgInt(i) = 0
    ELSE IF ((d_omega_temp(I) <= 1/2**0.5*cParm).and.(d_omega_temp(I) >= -cParm)) THEN
      d_omega(I) = rParm - (rParm**2 - (d_omega_temp(I) + cParm)**2)**0.5
      ldmgInt(i) = 1
    ELSE
      d_omega(I) = d_omega_temp(I)
      ldmgInt(i) = 2
    END IF

    IF (cParm.eq.0) THEN
      d_omega(I)=MAX(0.d0,d_omega_temp(I))
      ldmgInt(i) = 2
    END IF

    dmgInt(i) = dmgIntOld(i) + d_omega(I)

    IF ( dmgInt(i) > dmgIntMax ) THEN
      dmgInt(i) = dmgIntMax
      !ldmgInt(i) = 3
    END IF

    IF (E_INTFC_t(i)==0) THEN
      dmgInt(i) = 0.d0
      ldmgInt(i) = 2
      ldmgInt_c(i) = 2
    ENDIF

  END DO

!*****************************************************************
! Step(2): Compute ck = (1 - omega) * K + D
!*****************************************************************

!=========================================
! Eqn. 5.4: Oskay-Fish 2007
!=========================================
    ck = 0.d0
    ckNoDmg = 0.d0
    DO i = 1, rdInd
     ck(i,i) = 1.d0
    END DO

!=========================================
! Eqn. 5.6: Oskay-Fish 2007////
!=========================================
    DO i = 1, numint
       DO ii = 1, nsd
        i1 = rdInd + i + (ii-1)*numint
        ck(i1, i1) = ( 1.d0 - dmgInt(i) ) * IntPr(i)%ckInt(ii)
       END DO
    END DO


!=========================================
! Eqn. 5.7: Oskay-Fish 2007
!=========================================

    ck(rdInd+1:, rdInd+1:) = ck(rdInd+1:, rdInd+1:) + CT%de
    ckDerD = 0.d0 ! Partly numerical


!*****************************************************************
! Step(3): Compute derivative contribution
!*****************************************************************

!----------  Analytical Calculation of the interface damage derivative contribution

!  DO i = 1, numint
!    IF ( ldmgInt(i) .ne. 0.d0 .and. dmgInt(i) .lt. 1.d0 ) THEN
!      i1 = rdInd + i
!      tmpDer = IntPr(i)%a1Int / DATAN( IntPr(i)%a1Int * IntPr(i)%a2Int ) / &
!        ( 1.d0 + ( IntPr(i)%a1Int * eqvStnInt(i) )**2.d0  )

!      eqvStnIntD(1) = IntPr(i)%ckInt(1) *  DSIGN( 1.d0,  d( i1 ) )
!      phiDer(1) = tmpDer * eqvStnIntD(1)

!      DO j = 2, nsd
!        j1 = i1 + (j-1) * numint
!        IF (tngNorm(i).gt.1.d-20) THEN
!          eqvStnIntD(j) = IntPr(i)%ckInt(j) * d(j1) / tngNorm(i)
!          phiDer(j) = tmpDer * eqvStnIntD(j)
!        ELSE
!          phiDer(j) = 0.d0
!        END IF
!      END DO

!      IF (ldmgInt(i).eq.1) THEN
!        DO j = 1, nsd
!          dmgIntDer(j) = dmgIntDer(j) * (dmgInt_temp(i) + cParm) / (rParm**2 - (dmgInt_temp(i) + cParm)**2)**0.5
!        END DO
!      ELSE IF	(ldmgInt(i).eq.0) THEN
!        DO j = 1, nsd
!          dmgIntDer(j) = 0.d0
!        END DO
!      END IF

!      DO j = 1, nsd
!        j1 = i1 + (j-1) * numint
!        DO k = 1, nsd
!          k1 = i1 + (k-1) * numint
!          ckDerD(j1,k1) = - IntPr(i)%ckInt(j) * dmgIntDer(k) * d( j1 )
!        END DO
!      END DO

!    END IF
!  END DO

! Finite difference calculation of the interface damage derivative contribution
  if (mono_t==1) THEN
    phi_omega_gamma(I) = 1.d0
    phi_FD_temp(i) = dmgInt(i)
  else
    phi_omega_gamma(I) = (phi(I)/dmgInt(i))**INTPR(I)%gamma
    phi_FD_temp(i) = phi(i)
  endif

  DO i = 1, numint

    DO iDir = 1,nsd
      ! set the appropriate delta vector
      DO j = 1, nsd
        d_FD(j) = d((j-1)*numint + i )
      END DO
      ! perturb the appropriate term in the delta vector
      d_FD(iDir) = d_FD(iDir) + epsilon_FD

      ! Compute damage equivalent strain
      tngNorm_FD = 0.d0

      DO j = 2, nsd
        tngNorm_FD = tngNorm_FD + d_FD( j )**2.d0
      END DO

      tngNorm_FD = DSQRT( tngNorm_FD )
      eqvStnInt_FD = IntPr(i)%ckInt(1) * (DABS( d_FD(1) ) + d_FD(1)) / 2.d0 &
             + IntPr(i)%ckInt(2) * tngNorm_FD
      eqvStnInt_FD = DDIM( eqvStnInt_FD, IntPr(i)%eqvStnIniInt )
      phi_FD = DATAN( IntPr(i)%a1Int * eqvStnInt_FD ) / DATAN( IntPr(i)%a1Int * IntPr(i)%a2Int )
      dmgInt_FD = dmgInt(i) +  phi_omega_gamma(I) * (phi_FD - phi_FD_temp(i))

      if ( dmgInt(i) == 0) then
        dmgInt_FD = dmgInt(i)
      endif

      IF ( dmgInt_FD > dmgIntMax ) THEN
        dmgInt_FD = dmgIntMax
      END IF

      ! Compute the finite difference derivative
      dmgIntDer(iDir) = (dmgInt_FD - dmgInt(i)) / epsilon_FD
    END DO

    ! In the arc range of smoothed macaulay bracket
    ! d(omega)/df = d(omega)/d(omega_temp)*d(omega_temp)/df
    ! =(omega_temp-omega_old+c)/(r^2-(omega_temp-omega_old+c)^2)**d(omega_temp)/df
    IF (ldmgInt(i).eq.1) THEN
      DO j = 1, nsd
        dmgIntDer(j) = dmgIntDer(j) * (d_omega_temp(i) + cParm) / (rParm**2 - (d_omega_temp(i) + cParm)**2)**0.5
      END DO
    ELSEIF (ldmgInt(i).eq.0) THEN
      DO j = 1, nsd
        dmgIntDer(j) = 0.d0
      END DO
    END IF

    ! removing the nondiagonal components of the matrix
    ! The original is commented out above
    DO j = 1, nsd
      j1 = (j-1) * numint + i
      ckDerD(j1,j1) = - dmgIntDer(j) * IntPr(i)%ckInt(j) * d( j1 )
    END DO
  END DO


!*****************************************************************
! Step(4): Compute the penalty contribution func and derivative
!*****************************************************************

!=========================================
! Eqn. 5.10 & 5.40: Oskay-Fish 2007
!=========================================
  func = 0.d0
  funcDer = 0.d0
  DO i = 1, numint
     i1 = rdInd + i
     IF ( d( i1 ) .LT. 0.d0 ) THEN
        func(i1) = d(i1) / penParm
        funcDer(i1,i1) = 1.d0 / penParm
     END IF
  END DO

!***************************************************************
! Step(5): Compute the residual (fun) and jacobian (fjac)
!***************************************************************

!=========================================
! Eqn. 5.22: Oskay-Fish 2007
!=========================================
  fun = 0.d0
  fun = MATMUL( ck, d ) - rhs + func

!=========================================
! Eqn. 5.23 & 5.24: Oskay-Fish 2007
!=========================================

  fjac = ck + ckDerD + funcDer
  fjac1 = fjac

20 format( 100(E25.18,2x) )

END SUBROUTINE KFUNJAC


SUBROUTINE KNEWT_OMEGA(b,a,x,gamma)

  integer :: ITS
  REAL(KIND=8) :: x,xold,fun,fjac
  REAL(KIND=8) :: gamma,b,a
  LOGICAL :: check_omega
  PARAMETER ( maxits=5000,tolf=1.0d-12)

  !write(7,*) 'x_ini,a,b,gamma',x,a,b,gamma

  x_ini = x
  check_omega = .TRUE.
  fun = (x - a) * (x + a)**gamma - b
  fjac = (x - a) * gamma *((x + a) ** (gamma - 1)) + (x + a)**gamma
  IF ((DABS(fun)) < tolf) THEN
    check_omega = .false.
    RETURN
  ENDIF

  DO ITS = 1,MAXITS
    xold = x
    x = xold - fun/fjac
    fun = (x - a) * (x + a)**gamma - b
    fjac = (x - a) * gamma *((x + a) ** (gamma - 1)) + (x + a)**gamma
    IF ((DABS(fun)) < tolf) THEN
      check_omega = .false.
      RETURN
    ENDIF
  ENDDO

  !write(7,*) 'xini,a,b,gamma',x_ini,a,b,gamma
  !call xit

END SUBROUTINE KNEWT_OMEGA

