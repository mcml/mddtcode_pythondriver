SUBROUTINE URDFIL(LSTOP,LOVRWRT,KSTEP,KINC,DTIME,TIME)

	USE CALL_TERMINATION

      INCLUDE 'aba_param.inc'

      DIMENSION ARRAY(513),JRRAY(NPRECD,513),TIME(2)
      EQUIVALENCE (ARRAY(1),JRRAY(1,1))

	 !CALL POSFIL(KSTEP,KINC,ARRAY,JRCD)

	 !WRITE(*,*) 'here I am, termination'
	 !write(*,*) termination
	 IF (termination) THEN

	 	WRITE(*,*) 'maximum number of allowed spurious convergence'
     	WRITE(*,*) 'iterations exceeded: Aborting'

     	LSTOP=1

  	 END IF

RETURN

END SUBROUTINE URDFIL
