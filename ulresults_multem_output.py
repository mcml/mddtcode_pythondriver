"""
https://www.sharcnet.ca/Software/Abaqus610/Documentation/docs
/v6.10/books/cmd/default.htm?startat=pt05ch09s10.htmlodbMaxMises.py
Usage: abaqus python results_output.py CrossPly_S_quarter_5E-1
"""

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from odbAccess import *
from sys import argv,exit
import sys, os, re
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#==================================================================
# S T A R T
#

datapath = os.getcwd() + '/'
elsetName = 'PART-1-1'
var = 'SDV11'
elem = 1
frametotal = 17

argList = argv
argc = len(argList)
i=0

fvar = sys.argv[1] + var + '.dat'
if os.path.exists(fvar):
    fio = open(fvar,'a+')
else:
    fio = open(fvar,'w')

frame = range(0,frametotal+1)
print 'frame',frame
for n in frame:
    odbName = sys.argv[1] + '_fat_inelas' + str(n) + '.odb'
    print 'odbName',odbName
    odbpath = datapath + odbName

    if os.path.exists(odbpath):
        print "We've found the odb!"
    else:
        print "Can\'t find the odb!"
        sys.exit()

    elset = elemset = None
    """ Open the output database """
    odb = openOdb(odbName)
    assembly = odb.rootAssembly

    """ Check to see if the element set exists in the assembly"""

    if elsetName:
        try:
            elemset = assembly.instances[elsetName]
        except KeyError:
            print 'An assembly level instances named %s does' \
            'not exist in the output database %s' \
            % (elsetName, odbName)
            odb.close()
            exit(0)

    s = odb.steps.keys()[-1]
    laststep = odb.steps[s]
    frm=laststep.frames
    frmlast=frm[len(frm)-1]
    allFields=frmlast.fieldOutputs

    if (allFields.has_key(var)):
        varSet = allFields[var]
        if elemset:
            varvalue = varSet.getSubset(region=elemset).values
            #print 'varvalue',varvalue
            #vardata = []
            for varvalue2 in varvalue:
                if elem == varvalue2.elementLabel:
                    vardata = varvalue2.data
                    print 'vardata',vardata
                    fio.write(str(vardata)+'\n')
                #vardata.append(varvalue2.data)
            #fio.write(str(max(vardata))+'\n')
    odb.close()

fio.close()
