""" This file contains the functions that the various implementations of
temphom utilize """
import os
import sys
from datetime import datetime
import time
import re
import math

def write_micmac(i,name):
    """ Creates the micmac file for telling abaqus if we are a micro or macro
    step """
    fout = open(name+'micmac.dat','w')
    fout.write(str(i))
    fout.close()
    print 'Running write_micmac'

def clearfiles(jobname):
    """ Deletes job files not associated with the restart """
    os.system('/bin/rm '+jobname+'*023 &> /dev/null')
    os.system('/bin/rm '+jobname+'*lck &> /dev/null')
    os.system('/bin/rm '+jobname+'*sta &> /dev/null')
    os.system('/bin/rm '+jobname+'*sim &> /dev/null')
    os.system('/bin/rm '+jobname+'*OutD_*dat &> /dev/null')
    os.system('/bin/rm '+jobname+'*micmac.dat &> /dev/null')
    print 'Running clearfiles'

class job_info():
    """ This class stores all of the information associated with running the
    current job """
    def __init__(self,jobname):

        if not jobname.endswith('_'):
          jobname += '_'
        self.jobname = jobname

        # Multitemporal Job Options
        self.ncpu = '256'
        self.mpimode = 'mp_mode=mpi'
        self.restart = False
        self.gradual = 100
        self.startn = 1
        self.n_cycs = 100
        self.stress = 0
        self.make = True
        self.set_options()
        self.set_aliases()
        self.end = False
        self.threshold_var = 1
        self.dtol = 0.01
        self.tol = 2
        print 'Running _init_'
        print '------------------'


    def set_options(self):
        """ Define the options for the multitemporal analysis from the system
        arguments """
        # Options
        # n={number of cpus}
        # mpi={T/F} - false for shared memory and doubling cpus requested
        # grad={n} - number of cycles to ramp up to maximum
        # res - restart from semi=completed analysis
        # cycles={number of resolved cycles}
        # stress={loading amplitude} - Useful with gradual analysis to make sure the
        # correct amplitude is applied
        mpi = True
        print 'def set_options running'
        if len(sys.argv) > 2:
            for arg in sys.argv[2:]:
                arg = arg.lower()
                if arg.startswith('n='):
                    self.ncpu = arg.split('=')[-1]
                elif arg.startswith('mpi='):
                    if 'f' in arg.split('=')[-1]:
                        mpi = False
                        self.mpimode = ''
                elif arg.startswith('grad='):
                    self.gradual = int(arg.split('=')[-1])
                elif arg.startswith('res'):
                    self.restart = True
                elif 'nomake' in arg:
                    self.make = False
                elif arg.startswith('cycles='):
                    self.n_cycs = int(arg.split('=')[-1])
                elif arg.startswith('stress='):
                    self.stress = float(arg.split('=')[-1])
                    if self.stress < 3000.:
                        self.stress = self.stress*1e6 # Convert from MPa to Pa
                else:
                    print 'Bad argument: '+arg

        if mpi:
            self.mpimode = 'mp_mode=mpi'
        else:
            self.ncpu = str(2*int(self.ncpu))
            self.mpimode = ''
        print 'Running set_options'
        print '------------------'

    def set_aliases(self):
        # Pathing variables
        abaqusbase = '/cm/shared/apps/SIMULIA/Commands/abaqus'#os.environ['abaqus']
        self.abaqusbase = abaqusbase
        abaqus = abaqusbase + ' user=cpus=' + self.ncpu + ' ' + self.mpimode
        self.abaqus = abaqus
        self.workdir = os.getcwd()
        # Give absolute path to macrotemp so this script can be run from any directory
        # not just the one containing macrotemp
        adante_dir = os.path.dirname(os.path.realpath(__file__))
        self.macrotemp_dir = adante_dir + '/MDDTcode'
        self.combinepy = adante_dir + '/combine.py'
        self.jumppy = adante_dir + '/jump.py'
        print 'adante_dir=',adante_dir
        print 'Running set_aliases'
        print '------------------'


    """----------------------------------------------------------------------"""
    def cleanupoldjob(self):
        jn = self.jobname
        print 'Cleaning up old files for',jn
        # Calling rm, mv, and cp from bin so no options are automatically applied
        if self.restart:
            os.system('/bin/mv '+jn+'fat_inelas'+str(self.startn-1)+'.odb '+jn+str(self.startn-1)+'.odb')
            os.system('/bin/mv '+jn+'fat_inelas'+str(self.startn-1)+'.stt '+jn+str(self.startn-1)+'.stt')
            os.system('/bin/mv '+jn+'fat_inelas'+str(self.startn-1)+'.mdl '+jn+str(self.startn-1)+'.mdl')
            os.system('/bin/mv '+jn+'fat_inelas'+str(self.startn-1)+'.res '+jn+str(self.startn-1)+'.res')
            os.system('/bin/mv '+jn+'fat_inelas'+str(self.startn-1)+'.prt '+jn+str(self.startn-1)+'.prt')
            os.system('/bin/rm '+jn+'fat_* 2> /dev/null')
            os.system('/bin/mv '+jn+str(self.startn-1)+'.odb '+jn+'fat_inelas'+str(self.startn-1)+'.odb')
            os.system('/bin/mv '+jn+str(self.startn-1)+'.stt '+jn+'fat_inelas'+str(self.startn-1)+'.stt')
            os.system('/bin/mv '+jn+str(self.startn-1)+'.mdl '+jn+'fat_inelas'+str(self.startn-1)+'.mdl')
            os.system('/bin/mv '+jn+str(self.startn-1)+'.res '+jn+'fat_inelas'+str(self.startn-1)+'.res')
            os.system('/bin/mv '+jn+str(self.startn-1)+'.prt '+jn+'fat_inelas'+str(self.startn-1)+'.prt')
        else:
            os.system('/bin/rm '+jn+'fat_* 2> /dev/null')
        # Remove any existing results files or cycle information files
        os.system('/bin/rm -f '+jn+'cycles.dat &> /dev/null')
        os.system('/bin/rm -f '+jn+'var.dat &> /dev/null')
        os.system('/bin/rm -f '+jn+'DelD.dat &> /dev/null')
        os.system('/bin/rm -f '+jn+'results.txt &> /dev/null')
        print 'Running cleanupoldjobs'

    """----------------------------------------------------------------------"""
    def make_umat(self):
        # Clean up old .o and .so files
        print 'def make_umat running'
        os.chdir(self.macrotemp_dir)
        os.system('/bin/rm *.o 2> /dev/null')
        os.system('/bin/rm *.so 2> /dev/null')

        # Make the UMAT and move it to the analysis folder
        os.system('echo "Compile the UMAT - '+str(datetime.now())+'"')
        os.system(self.abaqusbase + ' make library=umat.f &> /dev/null')
        # Idle 30s to allow abaqus compile the .o and .so files
        time.sleep(30)
        while not os.path.exists('umat-std.o'):
            print 'Wait for umat to be accessible'
            time.sleep(10)
            # May cause an error in compiling that the .so file already exist
            # os.system(self.abaqusbase + ' make library=umat &> /dev/null')

        # Move the UMAT to the analysis folder
        os.system('/bin/cp umat-std.o ' + self.workdir + '/macro.o')

    """----------------------------------------------------------------------"""
    def initial_step(self):
        """ Run the initial macrotime step """
        # Create lock file
        os.system('echo "1" >> '+self.jobname+'is running')

        # Change to the work directory
        os.chdir(self.workdir)

        # Rename variables
        abaqusbase = self.abaqusbase
        abaqus = self.abaqus
        combinepy = self.combinepy
        jn = self.jobname
        newjob = jn + 'fat_inelas0'
        self.newjob = newjob

        # Delete existing job data for newjob.inp
        for l in os.listdir('.'):
            if l.startswith(newjob):
                os.system('/bin/rm -f '+l+' &> /dev/null')

        # Copy the inelas ic for the first fat_inelas iteration
        os.system('/bin/cp '+jn+'inelas.ic '+newjob+'.ic')

        # Run the first inelastic step
        command = abaqus+' job='+newjob+' input='+jn+\
          'inelas user=macro -inter'
        os.system('echo "Run initial loading cycle- '+str(datetime.now())+'"')
        print command
        os.system(command)#+' &> /dev/null')

        fname2 = jn + 'printtime.dat'
        fin = open(fname2,'w')
        fin.write('Run initial loading cycle- '+str(datetime.now())+'\n')
        fin.close()

        # Try to run the python script for accessing output results
        #try:
        #    command = abaqusbase + ' python '+jn+'out.py '+jn+' '+newjob
        #    os.system('echo "'+command+'"')
        #    os.system(command)
        #except:
        #    os.system('echo "No output results will be retrieved for '+newjob+'"')

        # Use combine.py to pull all of the OutD files together rename the file
        # so that it can be read by the next iteration
        # The generation of OutD file is in /macrotemp/uexternaldb.f90
        #os.system('/bin/cp OutD.dat ' + self.workdir + '/' + self.newjob+ 'OutD.dat')
        #command = 'python '+ combinepy +' '+self.newjob+'OutD'
        #print command
        #os.system(command)

        #command = '/bin/cp ' + newjob + '.odb ' + 'odbhist/' + newjob + '.odb'
        #os.system(command)
        #print command
        print ' ---------------------'
        print '|Running initial_step'
        print ' ---------------------'

    """----------------------------------------------------------------------"""
    def micro_step(self,n):
        """ Run the nth microstep for the job, no DelD needs to be read in """
        print ' ---------------------'
        print '|Running microstep',n
        print ' ---------------------'
        os.chdir(self.workdir)
        jn = self.jobname
        oldjob = jn + 'fat_inelas' + str(n-1)
        print 'oldjob=',oldjob
        newjob = jn + 'fat_elas' + str(n)
        print 'newjob=',newjob
        self.newjob = newjob

        # Delete old job data other than the DelD file
        # Should it be oldjob or newjob? question
        for l in os.listdir('.'):
            if l.startswith(newjob):
                os.system('/bin/rm -f '+l+' &> /dev/null')

        # Tell the UMAT to run micro step
        write_micmac(1,newjob)

        # Copy the input companion for the next elastic step
        base_er = jn + 'elasre'
        os.system('/bin/cp ' + base_er + '.ic ' + newjob + '.ic')

        # Run the elastic step restarting from the last inelastic step
        command = self.abaqus+' job=' + newjob
        command += ' input=' + base_er + ' user=macro oldjob=' + oldjob
        command += ' -inter'
        os.system('echo "----------------------Run microstep number '+str(n)+' - '+\
          str(datetime.now())+'----------------------"')
        os.system('echo '+command)
        #print command
        os.system(command)#+' &> /dev/null')
        time.sleep(30)

        # Read the .dat file from this job to see if there were any errors
        datfile = open(newjob+'.dat').read()
        # print 'datfile is ',datfile
        if ('***ERROR') in datfile:
            os.system('echo "Analysis Error in '+newjob+'!!"')
            self.end_job()
        else:
        # Remove the old elastic job files
            print 'No error found in .dat file for job ',newjob
            if n > 1:
                oldelas = jn + 'fat_elas' + str(n-1)
                for l in os.listdir('.'):
                    if l.startswith(oldelas) and not (l.endswith('odb') \
              or l.endswith('txt')):
                        os.system('rm -rf '+l)

        # Combine the DelD file written by the last microstep
        # os.system('/bin/cp OutD.dat ' + self.workdir + '/' + self.newjob+ 'OutD.dat') #zimu
        # command = 'python ' + self.combinepy + ' ' + newjob + 'OutD'
        # command = 'python ' + self.combinepy + ' ' + oldjob + 'OutD'
        #os.system('echo '+command)
        #os.system(command)
        #os.system('/bin/cp DelD.dat ' + self.workdir + '/' + self.newjob+ 'DelD.dat') #zimu
        #command = 'python ' + self.combinepy + ' ' + newjob + 'DelD'
        #os.system(command)
        #os.system('echo '+command)

        return 0

    """----------------------------------------------------------------------"""
    def read_cycles(self):
        """ Read the cycles.dat file for the current job """
        cfile = self.jobname + 'cycles.dat'
        print 'Running read_cycles'
        if os.path.exists(cfile):
            cyclines = open(cfile).readlines()
            cycs = [ int(f) for f in cyclines ]
        else:
            cycs = [1]
        return cycs

    """----------------------------------------------------------------------"""
    def read_results(self):
        """ Read the results.txt file for the current job """
        print 'Running read_results'
        try:
          reslines = open(self.jobname+'results.txt').readlines()
          e1 = float(reslines[0].split(';')[-2])
          s1 = float(reslines[0].split(';')[-1])
          e2 = float(reslines[-1].split(';')[-2])
          s2 = float(reslines[-1].split(';')[-1])
          print 'e1',e1
          print 's1',s1
          print 'e2',e2
          print 's2',s2
          E1 = s1/e1
          E2 = s2/e2
          gdamage = (1-E2/E1)
          os.system('echo "Global damage = '+str(gdamage)+'"')
          if gdamage > 0.75:
            os.system('echo "Global damage limit reached"')
            self.end_job()
          else:
            return gdamage
        except:
          os.system('echo "Results could not be read, continue"')
          return [0]

    """----------------------------------------------------------------------"""
    def jump(self,n):
        """ Determine the number of cycles to jump """
        print ' ---------------------'
        print '|Running Jump',n
        print ' ---------------------'
        os.system('echo "--------------Cycle jump starts--------------"')
        # Calculate the damage to jump from the DelD file written by the last microstep
        # and update the {jobname}cycles.dat file
        jn = self.jobname
        oldjob = jn + 'fat_elas' + str(n)
        newjob = jn + 'fat_inelas' + str(n)
        #command = 'python ' + self.jumppy +' '+ oldjob + ' ' + jn
        #os.system(command)

        # Read DelD
        fname = oldjob + 'DelD.dat'
        fin = open(fname)
        data = fin.read()
        fin.close()

        data = data.split()
        delds = []
        for num in data:
            delds.append(float(num))
        npt = len(delds)
        #print 'delds',delds
        deld_con_mean = sum(delds)/float(npt)
        deld_con_max = max(delds)

        fname2 = jn + 'DelD.dat'
        if os.path.exists(fname2):
            fpre = open(fname2)
            predata = fpre.read()
            fpre.close()
            predata = predata.split()
            predelds = []
            for num in predata:
                predelds.append(float(num))
            ddelds = []
            for i in range(len(delds)):
                ddelds.append(abs(delds[i] - predelds[i]))
            #print 'ddelds',ddelds
            fpre = open(fname2,'w')
            for ddeld in ddelds:
                fpre.write(str(ddeld)+'\n')
            fpre.close()
        else:
            fpre = open(fname2,'w')
            ddelds = delds
            for num in ddelds:
                fpre.write(str(num)+'\n')
            fpre.close()

        ddeld_con_mean = sum(ddelds)/float(npt)
        ddeld_con_max = max(ddelds)

        # Read in the cycles file
        fcycs = jn + 'cycles.dat'
        if os.path.exists(fcycs):
            fio = open(fcycs,'r+')
            data = fio.readlines()
            last = int(data[-1])
            predeln = int(data[-1]) - int(data[-2])
        else:
            fio = open(fcycs,'w')
            last = 1
            predeln = 1
            fio.write(str(last)+'\n')

        #deln = int(math.floor(dmean/deld_con_mean))
        if self.tol == 1:
            deln = int(math.floor(self.dtol/deld_con_max))
        if self.tol == 2:
            deln = int(math.floor(math.sqrt(self.dtol*2*predeln/ddeld_con_max)))

        if deln == 0:
            deln = 1

        print '--- JUMP ',deln,' cycles ----'
        print '--- max damage : ',deld_con_max,' ---'
        print '--- mean damage: ',deld_con_mean,' ---'

        #print 'deln',deln
        #print 'float(deln)',float(deln)
        delds2 = []
        for i in range(len(delds)):
            delds2.append(float(deln)*delds[i])
        #print 'ddelds',ddelds
        delds = delds2
        #print '--- damage of almost perdiocity : ',delds,' ---'

        fout = open(fname,'w')
        for deld in delds:
           fout.write(str(deld)+'\n')
        fout.close()

        fio.write(str(last+deln)+'\n')
        fio.close()

        cycs = self.read_cycles()
        os.system('echo "start at cycle ' + str(cycs[-2]) + \
            ', end at cycle ' + str(cycs[-1]) + '"')

        # Rename the DelD file to be used by the new job
        os.system('/bin/cp '+oldjob+'DelD.dat '+newjob+'DelD.dat')

    """----------------------------------------------------------------------"""
    def macro_step(self,n):
        """ Run the macrotime step for the analysis """
        print ' ---------------------'
        print '|Running macrostep',n,'|'
        print ' ---------------------'
        os.chdir(self.workdir)
        jn = self.jobname
        oldjob = jn + 'fat_inelas' + str(n-1)
        newjob = jn + 'fat_inelas' + str(n)
        self.newjob = newjob

        # Delete old job data other than the DelD file
        for l in os.listdir('.'):
            if l.startswith(newjob) and not l.endswith('DelD.dat'):
                os.system('/bin/rm -f '+l+' &> /dev/null')

        # Tell micmac to take a macro step
        write_micmac(0,newjob)

        # Copy the ic file
        os.system('/bin/cp '+ jn + 'inelasre.ic ' + newjob + '.ic')

        # Call Abaqus to run the next inelastic macro step - reads DelD and produces OutD
        command = self.abaqus + ' job=' + newjob
        command += ' input=' + jn + 'inelasre oldjob=' + oldjob
        command += ' user=macro -inter'
        os.system('echo "------------------------------Run macrostep number '+str(n)+\
          ' - '+str(datetime.now())+'----------------------------"')
        os.system('echo '+command)
        #print command
        os.system(command)#+' &> /dev/null')

        fname2 = jn + 'printtime.dat'
        fin = open(fname2,'a+')
        fin.write('Run macrostep number '+str(n)+' '+str(datetime.now())+'\n')
        fin.close()

        # Try to get the output results
        #try:
        #    command = self.abaqusbase + ' python '+jn+'out.py '+jn+' '+newjob
        #    os.system('echo "'+command+'"')
        #    os.system(command)
        #except:
        #    os.system('echo "No output results will be retrieved for '+jn+'"')

        #Read the .dat file from this job to see if there were any errors
        datfile = open(newjob+'.dat').read()
        if ('***ERROR') in datfile:
          os.system('echo "Analysis Error in '+newjob+'!!"')
          self.end_job()
        else:
        # Remove the old inelastic job files except the odb and .txt files
          oldinelas = jn + 'fat_inelas' + str(n-1)
          for l in os.listdir('.'):
            if l.startswith(oldinelas) and not (l.endswith('odb') \
              or l.endswith('res') or l.endswith('prt') \
              or l.endswith('mdl') or l.endswith('stt')):
              os.system('rm -rf '+l)

        return 0

    """ ----------------------------------------------------------------- """
    def check_continue(self,n):
        """ Read the results file and the cycles file to see if we could continue
        """
        print ' ---------------------'
        print '|Running check_continue',n
        print ' ---------------------'

        os.system('echo "--------------check critical value--------------"')
        jn = self.jobname
        abaqusbase = self.abaqusbase
        #check = self.read_results()

        newjob = self.newjob
        # Read in the cycles file
        cycs = self.read_cycles()
        cycles = cycs[-1]

        # Try to run the python script for accessing output results
        try:
            command = abaqusbase + ' python '+'results_output.py '+jn+' '+newjob
            os.system('echo "'+command+'"')
            os.system(command)
        except:
            os.system('echo "No output results will be retrieved for '+newjob+'"')

        fvar = jn + 'var.dat'
        if os.path.exists(fvar):
            fio = open(fvar,'r+')
            data = fio.readlines()
            last = float(data[-1])
            print ' ---------------------------'
            print '|the var has reached ',last
            print ' ---------------------------'
        else:
            print "there is no *var.dat"
            self.end_job()
        fio.close()

        if last == self.threshold_var:
            print 'the var has reached ',self.threshold_var
            self.end_job()
        return 0

    def end_job(self):
        """ delete the lock file """
        os.system('/bin/rm -f '+self.jobname+'is running')
        os.system('echo "End job signal has been received, quitting now"')
        self.end = True

    """ ------------------------------------------------------------------ """
    def check_restart(self,jobname):
        """ Check if the given jobname is suitable to restart from """
        n = re.findall('[0-9]*$',jobname)[0]

        if 'inelas' in jobname:
            # Check the micro and macro steps from the next multitemporal step
            macro = True
            np1 = str(int(n) + 1)
            nextjob = re.sub('inelas','elas',jobname)
            nextjob = re.sub(n,np1,nextjob)
            nextjob2 = re.sub(n,np1,jobname)
        else:
            macro = False
            # Check this steps macro step and the following micro step
            nextjob = re.sub('elas','inelas',jobname)
            np1 = str(int(n) + 1)
            nextjob2 = re.sub(n,np1,jobname)

        # For a macro step, check that the DelD file exists
        if macro:
            if not os.path.exists(jobname + 'DelD.dat'):
                return False

        # Check if the restart file exists for the current job
        if os.path.exists(jobname + '.res'):
        # Check if the restart file exists for the job 2 in the future (next one might
        # be incomplete)
            if not os.path.exists(nextjob2 + '.res'):
                return True # The next restart file might be incomplete,
                            # restart here
            else:
                return False # The next restart file is probably okay since theres
                         # one after it
        else:
            return False # The restart file exists so it won't work
    """ ---------------------------------------------------------------- """
    def find_restart(self):
        """ Go through the job files to figure out where to restart from """
        jn = self.jobname
        print 'Running find_restart'

        # Check the results and cycles files
        cycs = self.read_cycles()

        # Check the macroscopic steps for a suitable restart point
        macn = 0
        macname = jn + 'fat_inelas0'
        for n in range(self.n_cycs):
            macname = jn + 'fat_inelas' + str(n)
            maccheck = self.check_restart(macname)
            if maccheck:
                macn = n
                macres = macname
                print macres

        # Check the microscopic steps for a suitable restart point
        micn = 1
        micname = jn + 'fat_elas1'
        for n in range(1,self.n_cycs):
            micname = jn + 'fat_elas' + str(n)
            miccheck = self.check_restart(micname)
            if miccheck:
                micn = n
                micres = micname
                print micres

        # Decide where to restart from (returning the next job to be performed started
        # from the suitable restart point
        if macn >= micn:
        # Restart from the macroscopic step - run the next steps' micro analysis
            macro = False
            resn = macn + 1
        else:
        # Run this steps macro analysis
            macro = True
            resn = micn

        return macro, resn


    """ -------------------------------------------------------------- """
    def find_newdisp(self, appstress=0):
        """ Find the displacement and reaction stress the last step to
        determine the required displacement to reach the applied stress """
        print 'Running find_newdisp'
        if appstress == 0:
          appstress = self.set_stresslevel()

        os.system('echo "Set stress level to {0}"'.format(appstress))

        reslines = open(self.jobname + 'results.txt').readlines()
        # Read the displacement and stress at the last time step to
        # approximate the required displacement for the next step
        lastdisp = float(reslines[-1].split(';')[1])
        laststress = float(reslines[-1].split(';')[-1])

        newdisp = lastdisp/laststress*appstress;

        return newdisp

    """ ------------------------------------------------------- """
    def rewrite_disp(self):
        """ Write the displacement for the next job """
        print 'Running rewrite_disp'
        newdisp = self.find_newdisp()

        inelasre = self.jobname + 'inelasre.inp'
        elasre = self.jobname + 'elasre.inp'

        # Rewrite the displacement step in each of the input files
        match = r'\*[Dsload|Boundary].*\n.*\n'

        inelas_inp = open(inelasre).read()

        # Read the set that the boundary is applied on
        try:
          loadline = re.findall(match,inelas_inp)[0]
        except:
          os.system('echo "cannot find the loadline in the inelas file"')
        set = loadline.split('\n')[1].split(',')[0]

        # Rewrite the inelas restart file
        inelas = '*Boundary\n{0}, 3, 3, {1}\n'.format(set,newdisp)
        inelas_inp = re.sub(match,inelas,inelas_inp)
        open(inelasre,'w').write(inelas_inp)

        # Rewrite the elas restart file
        elas_inp = open(elasre).read()
        elas = '*Boundary, amplitude=Amp-1\n'+\
          '{0}, 3, 3, {1}\n'.format(set,newdisp)
        elas_inp = re.sub(match,elas,elas_inp)
        open(elasre,'w').write(elas_inp)
        print 'Set new displacement value to ',newdisp
        print '======================'

    """ ----- Functions for gradually increasing load ------------------- """
    def get_maxstress(self):
        """ Set the maximum stress amplitude either from twice the stress in
        the inelas.inp file or from the command line option """
        print 'Running get_maxstress'
        print '======================'
        if self.stress == 0:
        # Read from the inelas.inp file
            inp = self.jobname + 'inelas.inp'
            inpfile = open(inp).read()

            # Split the input file after the step declaration
            step = inpfile.split('*Step')[1]
            # Add second split to get to second step if first step is Preload
            # [0] indicates if you have 0 match, you'll get an error
            checkname = re.findall(r'name=Preload',step)#[0]
            if len(checkname) > 0:
                step = inpfile.split('*Step')[2]
            match = r'\*Dsload.*\n.*\n'
            loadlines = re.findall(match,step)[0]
            print loadlines

            loadline = loadlines.split('\n')[1]
            Pset, tmp, stress = loadline.split(',')
            self.stress = abs(float(stress)*2)

    def set_stresslevel(self):
        """ Find the stress level for the given load step from either the
        defined stress given as an option or twice the stress """
        cyc = self.read_cycles()[-1]
        print 'Running set_stresslevel'

        if self.gradual == 0:
            appstress = self.stress
        else:
            if cyc < self.gradual:
                appstress = self.stress*0.5*(1.0 + float(cyc)/float(self.gradual))
            else:
                appstress = self.stress

        print 'cyc',float(cyc)
        print 'self.stress',self.stress
        print 'self.gradual',self.gradual
        print 'appstress',appstress
        return appstress

    def write_stress_initial(self):
        """ Write the desired stress level to the initial inelas.inp file """
        inp = self.jobname + 'inelas.inp'
        inpfile = open(inp).read()

        front = inpfile.split('*Step')[0]
        step = inpfile.split('*Step')[1]
        # Add second split to get to second step if first step is Preload
        checkname = re.findall(r'name=Preload',step)#[0]
        if len(checkname) > 0:
            preload = step
            step = inpfile.split('*Step')[2]

        #front,step = inpfile.split('*Step')
        match = r'\*[Dsload|Boundary].*\n.*\n'

        try:
          loadline = re.findall(match,step)[0]
        except:
          os.system('echo "cannot find the loadline in the inelas file"')
        set = loadline.split('\n')[1].split(',')[0]


        inelas = '*Dsload\n{0}, P, -{1}\n'.format(set,self.stress/2)
        step = re.sub(match,inelas,step)

        if len(checkname) > 0:
            open(inp,'w').write(front + '*Step' + preload + '*Step' + step)
        else:
            open(inp,'w').write(front + '*Step' + step)
