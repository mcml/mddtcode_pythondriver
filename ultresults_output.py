"""
https://www.sharcnet.ca/Software/Abaqus610/Documentation/docs/v6.10/books/cmd/default.htm?startat=pt05ch09s10.html
odbMaxMises.py
Usage: abaqus python results_output.py CrossPly_S_quarter_5E-1
"""

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from odbAccess import *
from sys import argv,exit
import sys, os, re
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#==================================================================
# S T A R T
#    

datapath = os.getcwd() + '/'
elsetName = 'PART-1-1'
var = 'SDV11'
elem = 1
ncyc = 48
#ncyc is equals to n of last step plus 1.
#e.g. n=19, there is ncyc = 19+1

argList = argv
argc = len(argList)
i=0

fvar = sys.argv[1] + 'var_ult.dat'
fio = open(fvar,'w+')
fcycs = sys.argv[1] + '_cycles.dat'
if os.path.exists(fcycs):
    fcyc = open(fcycs,'r+')
    data = fcyc.readlines()
    fcyc.close()


for n in range(ncyc):
    odbName = sys.argv[1]+'_fat_'+'inelas'+str(n)+'.odb'
    odbpath = datapath + odbName

    if os.path.exists(odbpath):
        pass
    else:
        print "Can\'t find the odb!"
        sys.exit()

    elset = elemset = None
    """ Open the output database """
    odb = openOdb(odbName)
    assembly = odb.rootAssembly

    """ Check to see if the element set exists
    in the assembly
    """

    if elsetName:
        try:
            elemset = assembly.instances[elsetName]
            #print 'elemset',elemset
            #region = " in the instances set : " + elsetName;
        except KeyError:
            print 'An assembly level instances named %s does' \
                    'not exist in the output database %s' \
                    % (elsetName, odbName)
            odb.close()
            exit(0)

    s = odb.steps.keys()[-1]
    laststep = odb.steps[s]
    frm=laststep.frames
    frmlast=frm[len(frm)-1]
    allFields=frmlast.fieldOutputs


    if (allFields.has_key(var)):
        varSet = allFields[var]
        if elemset:
            varvalue = varSet.getSubset(region=elemset).values
            for varvalue2 in varvalue:
                if elem == varvalue2.elementLabel:
                    vardata = varvalue2.data
                    fio.write(str(int(data[n-ncyc]))+'  '+str(vardata)+'\n')
fio.close()       
odb.close() 