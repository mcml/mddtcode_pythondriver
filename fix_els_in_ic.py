#!/usr/bin/env python
""" Reads the input file and fixes the number of elements in the
corresponding .ic file accordingly"""
import sys,os,re

def fix_els_in_ic(jobname):

    if jobname.endswith('.inp'):
      jobname = jobname[:-4]

    lines = open(jobname+'.inp').readlines()

    inels = False
    innodes = False
    totalels = 0
    totalnodes = 0
    partels = {}
    partnodes = {}
    for line in lines:
      if line.startswith('*Part'):
        partname = line.split('=')[-1]
        noels = 0
        nonodes = 0
      if line.startswith('*') and not line.startswith('**'):
        inels = False
        innodes = False
      if line.startswith('*Element'):
        inels = True
        innodes = False
      if line.startswith('*Node'):
        innodes = True
        inels = False
      if inels and not line.startswith('*'):
        noels += 1
      if innodes and not line.startswith('*'):
        nonodes += 1

      if line.startswith('*End Part'):
        print 'Part',partname,' has ',noels,' elements'
        partels[partname]=noels
        partnodes[partname] = nonodes

      if line.startswith('*End Instance'):
        print 'Instance',insname,' has ',noels,' elements'
# Mesh is defined in the instance
        totalels += noels
        totalnodes += nonodes

      if line.startswith('*Instance'):
        noels = 0
        nonodes = 0
        insname = line.split('=')[-1]
        totalels += partels[insname]
        totalnodes += partnodes[insname]

    print 'Total number of elements: ',totalels
    print 'Total number of nodes: ',totalnodes

	
    iclines = open(jobname+'.ic').read()
    print 'jobname',jobname
    #print 'iclines',iclines
    print 'str(totalels)',str(totalels)
    newic = re.sub('\*noel\n.*','*noel\n'+str(totalels),iclines)
    #print 'newic',newic

    open(jobname+'.ic','w').write(newic)

#if len(sys.argv) > 1:
#    jobname = sys.argv[1]
#    fix_els_in_ic(jobname)

